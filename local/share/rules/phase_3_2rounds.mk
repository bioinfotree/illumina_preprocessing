# Copyright 2015 Michele Vidotto <michele.vidotto@gmail.com>

extern ../phase_2.1/phase.end.flag as PHASE2_END

LINKER ?= CTGTCTCTTATACACATCT

# this function install all the links at once
1_CLEAN_GZ = $(addsuffix .1.fastq.gz,$(SAMPLES))
2_CLEAN_GZ = $(addsuffix .2.fastq.gz,$(SAMPLES))
%.1.fastq.gz %.2.fastq.gz: $(PHASE2_END)
	@echo installing links ... \
	$(foreach SAMPLE,$(SAMPLES), \
		$(shell ln -sf ../phase_2.1/$(SAMPLE).cutadapt.erne_1.fastq.gz $(SAMPLE).1.fastq.gz) \
		$(shell ln -sf ../phase_2.1/$(SAMPLE).cutadapt.erne_2.fastq.gz $(SAMPLE).2.fastq.gz) \
	)

import phase_3_common