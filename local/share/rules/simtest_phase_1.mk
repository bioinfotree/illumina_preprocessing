# Copyright 2013 Michele Vidotto <michele.vidotto@gmail.com>

SEQ ?=
USED_SEQ ?=
COV ?=
LEN ?=
FRAG ?=
STDEV ?=
ADAPTOR1 ?=
ADAPTOR2 ?=
SHORT_RATIO ?=
CONTAMINANTS ?=

SHORT_LEN = $(shell echo "$(LEN)/100 * $(SHORT_RATIO)" | bc)


ref.fasta.gz:
	ln -sf $(SEQ) $@

contaminants.fasta.gz:
	ln -sf $(CONTAMINANTS) $@


define sanitize
	zcat $1 \
	| sed -e 's/*//' -e '/^$$/d' \   * remove pad, empty lines *
	| fasta2tab \
	| bsort --key=1,1 \
	| bawk -v min_len=$(MIN_LEN) -v used_seq=$(USED_SEQ) '!/^[$$,\#+]/ { \
	if ( ( length($$2) > min_len ) && (NR <= used_seq) )  \   * sequence too short are discarged since art do Segmentation fault (core dumped) those sequences *
	{ print $$0; } }' \
	| tab2fasta 2 >$2
endef

# sanitizes the fasta files
ref.sane.fasta: ref.fasta.gz
	$(call sanitize,$<,$@)

contaminants.sane.fasta: contaminants.fasta.gz
	$(call sanitize,$<,$@)



# simulate PAIRED-END reads
noadapt_reads_1.fq: ref.sane.fasta
	$(call module_loader); \
	art_illumina -p -l $(LEN) -f $(COV) -m $(FRAG) -s $(STDEV) -d 'NOADAPT_' -o $(call first,$(call split ,1,$@)) -i $<

noadapt_reads_2.fq: noadapt_reads_1.fq
	touch $@

# simulate PAIRED-END reads + ADAPTORS
define add-adaptors
	fastq2tab <$1 \
	| bawk -v p1=$(ADAPTOR1) -v p2=$(ADAPTOR2) '!/^[$$,\#+]/ { \
	print "read_" NR,p1 $$2 p2,$$3; }' \
	| cut -f 1,2 \
	| bsort \
	| tab2fasta 2 >$2
endef

noadapt_reads_%.fasta: noadapt_reads_%.fq
	$(call add-adaptors,$<,$@)



adapt_reads_%.fq: noadapt_reads_%.fasta
	$(call module_loader); \
	ADAPTOR1=$(ADAPTOR1); \
	ADAPTOR2=$(ADAPTOR2); \
	LEN=$$(echo "$(LEN) + $${#ADAPTOR1} + $${#ADAPTOR2} -1" | bc); \
	art_illumina -l $$LEN -f 1 --noALN -ir 0 -dr 0 -ir2 0 -dr2 0 -d 'ADAPT_' -o $(basename $@) -i $<







# simulate CONTAMINANT reads + ADAPTORS
noadapt_contaminants_1.fq: contaminants.sane.fasta
	$(call module_loader); \
	art_illumina -p -l $(LEN) -f $(COV) -m $(FRAG) -s $(STDEV) -d 'NOADAPT_CONTAMINANTS_' -o $(call first,$(call split ,1,$@)) -i $<

noadapt_contaminants_2.fq: noadapt_contaminants_1.fq
	touch $@



noadapt_contaminants_%.fasta: noadapt_contaminants_%.fq
	$(call add-adaptors,$<,$@)

contaminants_%.fq: noadapt_contaminants_%.fasta
	$(call module_loader); \
	ADAPTOR1=$(ADAPTOR1); \
	ADAPTOR2=$(ADAPTOR2); \
	LEN=$$(echo "$(LEN) + $${#ADAPTOR1} + $${#ADAPTOR2} -1" | bc); \
	art_illumina -l $$LEN -f 1 --noALN -ir 0 -dr 0 -ir2 0 -dr2 0 -d 'CONTAMINANTS_' -o $(basename $@) -i $<

##########################################


# simulate CONCATAMERS OF PRIMERS reads
tmp_concatadapt_reads_1.fq: ref.sane.fasta
	$(call module_loader); \
	art_illumina -p -l $(LEN) -f $(COV) -m $(FRAG) -s $(STDEV) -d 'CONCATADAPT_' -o $(call first,$(call split ,1,$@)) -i $<

tmp_concatadapt_reads_2.fq: tmp_concatadapt_reads_1.fq
	touch $@

tmp_concatadapt_reads_%.fasta: tmp_concatadapt_reads_%.fq
	fastq2tab <$< \
	| bawk -v p1=$(ADAPTOR1) -v p2=$(ADAPTOR2) '!/^[$$,\#+]/ { \
	print "read_" NR,p1 p2; }' \
	| cut -f 1,2 \
	| bsort \
	| tab2fasta 2 >$@


concatadapt_reads_%.fq: tmp_concatadapt_reads_%.fasta
	$(call module_loader); \
	ADAPTOR1=$(ADAPTOR1); \
	ADAPTOR2=$(ADAPTOR2); \
	LEN=$$(echo "$${#ADAPTOR1} + $${#ADAPTOR2} -1" | bc); \
	art_illumina -l $$LEN -f 1 --noALN  -ir 0 -dr 0 -ir2 0 -dr2 0 -d 'CONCATADAPT_' -o $(basename $@) -i $<







# simulate TOO SHORT reads
tmp_short_reads_1.fq: ref.sane.fasta
	$(call module_loader); \
	art_illumina -p -l $(SHORT_LEN) -f $(COV) -m $(FRAG) -s $(STDEV) -d 'SHORT_' -o $(call first,$(call split ,1,$@)) -i $<

tmp_short_reads_2.fq: tmp_short_reads_1.fq
	touch $@

tmp_short_reads_%.fasta: tmp_short_reads_%.fq
	fastq2tab <$< \
	| bawk -v p1=$(ADAPTOR1) -v p2=$(ADAPTOR2) '!/^[$$,\#+]/ { \
	print "read_" NR,p1 $$2 p2, $$3; }' \
	| cut -f 1,2 \
	| bsort \
	| tab2fasta 2 >$@

short_reads_%.fq: tmp_short_reads_%.fasta
	$(call module_loader); \
	ADAPTOR1=$(ADAPTOR1); \
	ADAPTOR2=$(ADAPTOR2); \
	LEN=$$(echo "$(SHORT_LEN) + $${#ADAPTOR1} + $${#ADAPTOR2} -1" | bc); \
	art_illumina -l $$LEN -f 1 --noALN  -ir 0 -dr 0 -ir2 0 -dr2 0 -d 'SHORT_' -o $(basename $@) -i $<





# join
reads_%.fq.gz: noadapt_reads_%.fq adapt_reads_%.fq concatadapt_reads_%.fq short_reads_%.fq contaminants_%.fq
	$(call module_loader); \
	>stats_$*; \
	for FILE in $^; do \
	SEQ=$$(fastq2tab <$$FILE | wc -l); \
	LEN=$$(fastq_to_fasta -Q 33 <$$FILE | fasta_length | stat_base -a 2); \
	printf "%s\t%i\t%.2f\n" $$FILE $$SEQ $$LEN >>stats_$*; \
	done; \
	cat $^ \
	| gzip -c >$@


stats_%: reads_%.fq.gz
	touch $@


# for testing purpose
.PHONY: test
test:
	@echo $(FQ1)



ALL +=  ref.fasta.gz \
	contaminants.fasta.gz \
	reads_1.fq.gz \
	reads_2.fq.gz \
	stats_1 \
	stats_2


INTERMEDIATE += $(wildcard *.fq)


CLEAN += $(wildcard *.fasta) \
	 $(wildcard *.aln)

######################################################################
### rules ends here
