# Copyright 2013 Michele Vidotto <michele.vidotto@gmail.com>

# TODO:
#	add generation of bigWig from Wig file in coverage.sh, using wigToBigWig utility from sw/bio/ucsc-utils/default
#	why was used samtools import for target same.chr.bam


# @RG Read group. Unordered multiple @RG lines are allowed.
# Read group identier. Each @RG line must have a unique ID. The value of ID is used in the RG
# 	tags of alignment records. Must be unique among all read groups in hRead group_IDeader section. Read group
# 	IDs may be modied when merging SAM les in order to handle collisions.
READ_GROUP_ID ?= TRAMINER
# 	CN Name of sequencing center producing the read.
READ_GROUP_CN ?= IGA
# 	DS Description.
READ_GROUP_DS ?= TRAMINER
# 	DT Date the run was produced (ISO8601 date or date/time).
READ_GROUP_DT ?= 1997-07-16T19:20+01:00
# 	FO Flow order. The array of nucleotide bases that correspond to the nucleotides used for each
# 	ow of each read. Multi-base ows are encoded in IUPAC format, and non-nucleotide ows by
# 	various other characters. Format: /\*|[ACMGRSVTWYHKDBN]+/
READ_GROUP_FO ?= 1
# 	KS The array of nucleotide bases that correspond to the key sequence of each read.
READ_GROUP_KS ?= 1
# 	LB Library.
READ_GROUP_LB ?= TRAMINER
# 	PG Programs used for processing the read group.
READ_GROUP_PG ?= BWA-MEM
# 	PI Predicted median insert size.
READ_GROUP_PI ?= 0
# 	PL Platform/technology used to produce the reads. Valid values: CAPILLARY, LS454, ILLUMINA, SOLID, HELICOS,IONTORRENT and PACBIO.
READ_GROUP_PL ?= ILLUMINA
# 	PU Platform unit (e.g. owcell-barcode.lane for Illumina or slide for SOLiD). Unique identifer.
READ_GROUP_PU ?= 001
# 	SM Sample. Use pool name where a pool is being sequenced
READ_GROUP_SM ?= TRAMINER

extern ../phase_2/nocontam.noadapt.trimmed_1.fastq.gz			as PAIRED1
extern ../phase_2/nocontam.noadapt.trimmed_2.fastq.gz			as PAIRED2
extern ../phase_3/total.nocontam.noadapt.trimmed_unpaired.fastq.gz	as UNPAIRED

extern ../phase_5/reference.fasta.*					as INDEX

# for testing purpose
.PHONY: test
test:
	@echo

paired.1.fastq.gz:
	ln -sf $(PAIRED1) $@

paired.2.fastq.gz:
	ln -sf $(PAIRED2) $@

unpaired.fastq.gz:
	ln -sf $(UNPAIRED) $@

reference.fasta:
	ln -sf $(REFERENCE) $@

logs:
	mkdir -p $@

reference.fasta.bwt:
	$(call module_loader); \
	for FILE in $(INDEX); do \
	   ln -sf $$FILE $$(basename $$FILE); \
	done

paired.%.fastq: paired.%.fastq.gz
	zcat $< >$@

unpaired.fastq: unpaired.fastq.gz
	zcat $< >$@

# align paired reads
paired.sam: reference.fasta.bwt paired.1.fastq paired.2.fastq logs
	!threads
	$(call module_loader); \
	bwa mem \
	-t $$THREADNUM \   * number of threads *
	-M \   * Mark shorter split hits as secondary (for Picard compatibility) *
	-R "@RG\tID:$(READ_GROUP_ID)\tCN:$(READ_GROUP_CN)\tDS:$(READ_GROUP_DS)\tDT:$(READ_GROUP_DT)\tFO:$(READ_GROUP_FO)\tKS:$(READ_GROUP_KS)\tLB:$(READ_GROUP_LB)\tPG:$(READ_GROUP_PG)\tPI:$(READ_GROUP_PI)\tPL:$(READ_GROUP_PL)\tPU:$(READ_GROUP_PU)\tSM:$(READ_GROUP_SM)" \   * Complete read group header line. *
	$(basename $<) \   * db prefix *
	$^2 \   * reads 1 *
	$^3 \   * reads 2 *
	2>$@ 3>&1 1>&2 2>&3 \   * redirect stdout to file, stderr to stdout and file *
	| tee $^4/bwa-mem.$@.log   * [tool].[taget].log *


# align unpaired reads
unpaired.sam: reference.fasta.bwt unpaired.fastq logs
	!threads
	$(call module_loader); \
	bwa mem \
	-t $$THREADNUM \   * number of threads *
	-M \   * Mark shorter split hits as secondary (for Picard compatibility) *
	-R "@RG\tID:$(READ_GROUP_ID)\tCN:$(READ_GROUP_CN)\tDS:$(READ_GROUP_DS)\tDT:$(READ_GROUP_DT)\tFO:$(READ_GROUP_FO)\tKS:$(READ_GROUP_KS)\tLB:$(READ_GROUP_LB)\tPG:$(READ_GROUP_PG)\tPI:$(READ_GROUP_PI)\tPL:$(READ_GROUP_PL)\tPU:$(READ_GROUP_PU)\tSM:$(READ_GROUP_SM)" \   * Complete read group header line. *
	$(basename $<) \   * db prefix *
	$^2 \   * reads 1 *
	2>$@ 3>&1 1>&2 2>&3 \
	| tee $^3/bwa-mem.$@.log   * [tool-subtool].[taget].log *


# converts sam to bam
define sam2bam
$(call module_loader); \
samtools view \
-bS \
$1 \
2>$2/samtools-view.$@.log \
| samtools sort -@ $$THREADNUM - $(basename $3)   * with -o option samtools sort need out file prefix the same *
endef


# converts sam to bam, sort and fix mate coordinates, ISIZE and mate related flags
%.bam: %.sam logs
	!threads
	$(call sam2bam,$<,$^2,$@)

# unpaired.bam: unpaired.sam logs
# 	!threads
# 	$(call sam2bam,$<,$^2,$@)

# merge all bam files
# alignment.bam: paired.bam unpaired.bam logs
# 	$(call module_loader); \
# 	java_exec MergeSamFiles.jar \
# 	USE_THREADING=true \
# 	INPUT=$< \
# 	INPUT=$^2 \
# 	OUTPUT=$@ \
# 	VALIDATION_STRINGENCY=LENIENT \
# 	3>&1 1>&2 2>&3 \
# 	| tee $^3/picard-MergeSamFiles.$@.log



# 	!threads
# 	$(call module_loader); \
# 	samtools merge -f \
# 	- $< $^2 2>$^3/samtools-merge.$@.log \
# 	| samtools sort -@ $$THREADNUM - $(basename $@)


# remove PCR duplicates
# try mark duplicates of picardtools
nodup.%.bam: %.bam logs
	!threads
	$(call module_loader); \
	samtools rmdup -s $< - 2>$^2/samtools-rmdup.$@.log \
	| samtools sort -@ $$THREADNUM - $(basename $@)


# collect metrics of alignment by recalling other tools 

# CollectAlignmentSummaryMetrics:	calculate summary alignment metrics 
# CollectInsertSizeMetrics:		calculate metrics about the statistical distribution of insert size (excluding duplicates) and generates a histogram plot
# QualityScoreDistribution:		chart quality score distributions 
# MeanQualityByCycle:			calculate mean quality at every cicle
%_summary_metrics: reference.fasta %.bam logs
	$(call module_loader); \
	java_exec CollectMultipleMetrics.jar \
	REFERENCE_SEQUENCE=$< \
	INPUT=$^2 \
	OUTPUT=$(basename $@) \
	VALIDATION_STRINGENCY=LENIENT \
	3>&1 1>&2 2>&3 \
	| tee $^3/picard-CollectMultipleMetrics.$@.log


# select alignments with mapping quality more then 20 and that are primary

# The correct placement of a read may be ambiguous, e.g. due to repeats. In
# this case, there may be multiple read alignments for the same read. One of these alignments is
# considered primary. All the other alignments have the secondary alignment ag set in the SAM
# records that represent them.
uniq.nodup.%.bam: nodup.%.bam
	$(call module_loader); \
	samtools view \
	-bhq 20 \
	-F '0x0100' \   * skip alignment that are not primary *
	$< >$@


# select only reads that maps to same chr in order to prevent problems with Breakdancer (caller for deletions).
# Reads that map in different chrs are not usefull for the identification of deletions with Breakdancer
# The 7th field of sam/bam indicate the reference name on which the paired sequence align.
# If it is the same as for the first reads this field became ‘=’ 
same.chr.uniq.nodup.%.bam: uniq.nodup.%.bam
	$(call module_loader); \
	samtools view -h $< \
	| bawk '!/^[$$,\#+]/ { if (  $$1 ~ /^@/  \   * in order to include header *
	|| $$7 == "=" ) print $$0; }' \
	| samtools view -bhS - >$@

 
# selection only reads that are mapped in a proper pair. See bwa man.
ppaired.uniq.nodup.%.bam: uniq.nodup.%.bam
	$(call module_loader); \
	samtools view -f '0x0002' -hb $< >$@

# calcolate genome size, genome really coverad and coverage of genome really covered
%.cov: reference.fasta %.bam
	$(call module_loader); \
	coverage -f $< $^2 >$@



ALL +=	paired.1.fastq.gz \
	paired.2.fastq.gz \
	unpaired.fastq.gz \
	logs \
	reference.fasta \
	reference.fasta.bwt \
	paired.bam \
	nodup.paired.bam \
	uniq.nodup.paired.bam \
	same.chr.uniq.nodup.paired.bam \
	ppaired.uniq.nodup.paired.bam \
	uniq.nodup.paired_summary_metrics \   * get metrics only for not duplicated and uniquely mapped reads *
	paired.cov \
	nodup.paired.cov \
	uniq.nodup.paired.cov \
	same.chr.uniq.nodup.paired.cov \
	ppaired.uniq.nodup.paired.cov \
	unpaired.bam \
	nodup.unpaired.bam \
	uniq.nodup.unpaired.bam \
	same.chr.uniq.nodup.paired.bam \   * calculate properly paired reads does not make sense for unpaired *
	uniq.nodup.unpaired_summary_metrics \
	unpaired.cov \
	nodup.unpaired.cov \
	uniq.nodup.unpaired.cov \
	same.chr.uniq.nodup.paired.cov




INTERMEDIATE += paired.1.fastq \
		paired.2.fastq \
		unpaired.fastq \
		paired.sam \
		unpaired.sam


CLEAN += logs \
	 $(wildcard reference.fasta.*) \
	 $(wildcard *.pdf) \
	 $(wildcard *_metrics)




