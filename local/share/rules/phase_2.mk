# Copyright 2015 Michele Vidotto <michele.vidotto@gmail.com>

extern ../phase_1/contaminants.eht as CONTAMINANT_INDEXES

MIN_OVERLAP_LEN ?= 10
MIN_SIZE ?= 35

log:
	mkdir -p $@

contaminants.eht:
	ln -sf $(CONTAMINANT_INDEXES) $@


# this function install all the links at once
1_FASTQ_GZ = $(addsuffix .1.fastq.gz,$(SAMPLES))
2_FASTQ_GZ = $(addsuffix .2.fastq.gz,$(SAMPLES))
AD = $(addsuffix .adapters,$(SAMPLES))

%.adapters %.1.fastq.gz %.2.fastq.gz:
	@echo installing links ... \
	$(foreach SAMPLE,$(SAMPLES), \
		$(shell ln -sf ../phase_1/$(SAMPLE).1.dc.fastq.gz $(SAMPLE).1.fastq.gz) \
		$(shell ln -sf ../phase_1/$(SAMPLE).2.dc.fastq.gz $(SAMPLE).2.fastq.gz) \
		$(shell echo -e '$(call get,$(SAMPLE),AD1)\t$(call get,$(SAMPLE),AD2)' >$(SAMPLE).adapters) \
	) \
	&& sleep 3

# [PIPELINE]
# name: Trimming_PE_new_order
# codename: trimming-pe-new-order
# code: 0b9d3bac-d7a7-43a0-9917-6b87ed7fa024
# [step.s1]
# label: cutadapt1
define cutadapt
	$(call load_modules); \
	cutadapt \
	--adapter "$$(cat $2 | cut -f1)" \   * 3' end of read *
	--adapter "$$(cat $2 | cut -f2)" \   * 3' end of read *
	--format fastq \
	--overlap $(MIN_OVERLAP_LEN) \
	--times 4 \
	--mask-adapter \
	--output $4 \
	$3 \
	| tee $1/cutadapt.$4.log.tmp
endef


1_CUTADAPT = $(addsuffix .1.cutadapt.gz,$(SAMPLES))
%.1.cutadapt.gz: log %.adapters %.1.fastq.gz 
	$(call cutadapt,$<,$^2,$^3,$@)

2_CUTADAPT = $(addsuffix .2.cutadapt.gz,$(SAMPLES))
%.2.cutadapt.gz: log %.adapters %.2.fastq.gz 
	$(call cutadapt,$<,$^2,$^3,$@)


1_CUTADAPT_LOG_TMP = $(addprefix log/cutadapt.,$(addsuffix .1.cutadapt.gz.log.tmp,$(SAMPLES)))
log/cutadapt.%.1.cutadapt.gz.log.tmp: %.1.cutadapt.gz
	touch $@

2_CUTADAPT_LOG_TMP = $(addprefix log/cutadapt.,$(addsuffix .2.cutadapt.gz.log.tmp,$(SAMPLES)))
log/cutadapt.%.2.cutadapt.gz.log.tmp: %.2.cutadapt.gz
	touch $@



.META: cutadapt.%.log
	1	sample name
	2	Processed reads
	3	trimmed reads
	4	trimmed reads %
	5	passing filters
	6	passing filters %
	7	Total basepairs processed
	8	Total basepairs written
	9	Total basepairs written %
	10	3 prime end adapter 1
	11	num. Trim
	12	3 prime end adapter 2
	13	num. Trim


CUTADAPT_LOG = $(addprefix cutadapt.,$(addsuffix .log,$(SAMPLES)))
cutadapt.%.log: log/cutadapt.%.1.cutadapt.gz.log.tmp log/cutadapt.%.2.cutadapt.gz.log.tmp
	( \
	for FILE in $^; do \
		TMP=$$(basename "$$FILE"); \
		TMP1=$${TMP%.cutadapt.gz.log.tmp}; \
		paste -d "\t" \
		<(bawk '/^=== Summary/{flag=1;next}/^=== Adapter 1/{flag=0}flag { if ( $$0 != "" ) print $$0; }' <$$FILE \
		| sed 's/ \{2,\}/\t/' \
		| cut -f2 \
		| sed -e 's/ bp//' \
		| tr -d '(),' \
		| transpose \
		| tr ' ' \\t) \
		<(bawk '/^=== Adapter [0-9]/{flag=1;next}/^No./{flag=0}flag { print $$0; }' <$$FILE \
		| sed '/^$$/d' \
		| tr \\n " " \
		| bawk '!/^[\#+,$$]/ { \
		gsub(";", "", $$0); \
		split($$0, a, " "); \
		print a[2], a[9], a[12], a[19], a[23], a[31], a[35], a[42]; }') \
		| sed "s/^/$$TMP1\t/"; \
	done \
	) >$@

		#for (i=0;i<length(a);i++) { print i, a[i]; }; \
		#print a[2], a[9], a[11], a[21], a[34], a[41], a[43], a[53];}' \



1_CA_CLEAN = $(addsuffix .1.cutadapt.cleaned.fastq.gz,$(SAMPLES))
%.1.cutadapt.cleaned.fastq.gz: log %.1.cutadapt.gz %.2.cutadapt.gz
	$(call load_modules); \
	clean_removed_adapter \
	--R1 $^2 \
	--R2 $^3 \
	-v \
	-o "clean_removed_adapter.$*" \
	2>&1 \
	| tee $</clean_removed_adapter.$@.log \
	&& mv clean_removed_adapter.$*_1.fastq.gz $@ \
	&& mv clean_removed_adapter.$*_2.fastq.gz $*.2.cutadapt.cleaned.fastq.gz

2_CA_CLEAN = $(addsuffix .2.cutadapt.cleaned.fastq.gz,$(SAMPLES))
%.2.cutadapt.cleaned.fastq.gz: %.1.cutadapt.cleaned.fastq.gz
	touch $@





# [PIPELINE]
# name: Trimming_PE_new_order
# codename: trimming-pe-new-order
# code: 0b9d3bac-d7a7-43a0-9917-6b87ed7fa024
# [step.s4]
# label: erne-filter
#	--contamination-reference $^4
#	--no-indels
1_ERNE = $(addsuffix .cutadapt.erne_1.fastq.gz,$(SAMPLES))
%.cutadapt.erne_1.fastq.gz: log %.1.cutadapt.cleaned.fastq.gz %.2.cutadapt.cleaned.fastq.gz contaminants.eht
	!threads
	$(call load_modules); \
	erne-filter \
	--query1 $^2 \
	--query2 $^3 \
	--min-size $(MIN_SIZE) \
	--threads $$THREADNUM \
	--gzip \
	--output-prefix "$*.cutadapt.erne" \
	| tee $</erne-filter.$*.log.tmp


2_ERNE = $(addsuffix .cutadapt.erne_2.fastq.gz,$(SAMPLES))
UNPAIRED_ERNE = $(addsuffix .cutadapt.erne_unpaired.fastq.gz,$(SAMPLES))

%.cutadapt.erne_unpaired.fastq.gz %.cutadapt.erne_2.fastq.gz: %.cutadapt.erne_1.fastq.gz
	touch $@

ERNE_LOG_TMP = $(addprefix log/erne-filter.,$(addsuffix .log.tmp,$(SAMPLES)))

log/erne-filter.%.log.tmp: %.cutadapt.erne_1.fastq.gz
	touch $@

.META: erne-filter.%.log
	1	sample
	2	reads
	3	good reads
	4	discarded reads
	5	contaminated reads
	6	good bases

ERNE_LOG = $(addprefix erne-filter.,$(addsuffix .log,$(SAMPLES)))
erne-filter.%.log: log/erne-filter.%.log.tmp
	bawk '/^\# / { \
		split($$0, a, ":"); \
		gsub(" ", "", $$0); \
		print a[2]; }' $< \
	| transpose \
	| sed 's/^/$*\t/' >$@


phase.end.flag: $(1_ERNE) $(2_ERNE) $(UNPAIRED_ERNE) $(ERNE_LOG)
	touch $@

import move_file

.PHONY: test
test:
	@echo $(1_ERNE) $(2_ERNE) $(UNPAIRED_ERNE)



ALL += log \
	$(1_FASTQ_GZ) \
	$(2_FASTQ_GZ) \
	$(AD) \
	$(CUTADAPT_LOG) \
	$(1_CUTADAPT_LOG_TMP) $(2_CUTADAPT_LOG_TMP) \
	$(1_ERNE) $(2_ERNE) $(UNPAIRED_ERNE) \
	$(ERNE_LOG) \
	phase.end.flag

INTERMEDIATE += $(1_CUTADAPT) $(2_CUTADAPT) \
	$(1_CA_CLEAN) $(2_CA_CLEAN)

CLEAN += contaminants.eht
