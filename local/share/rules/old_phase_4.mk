# Copyright 2013 Michele Vidotto <michele.vidotto@gmail.com>

# TODO:
#

LINKER ?= CTGTCTCTTATACACATCT

context prj/kmer_count

extern ../phase_2/nocontam.noadapt.trimmed_1.fastq.gz		as PAIRED1
extern ../phase_2/nocontam.noadapt.trimmed_2.fastq.gz		as PAIRED2
extern ../phase_3/nocontam.noadapt.trimmed_unpaired.fastq.gz	as UNPAIRED

# for testing purpose
.PHONY: test
test:
	@echo

paired.1.fastq.gz:
	ln -sf $(PAIRED1) $@

paired.2.fastq.gz:
	ln -sf $(PAIRED2) $@

unpaired.fastq.gz:
	ln -sf $(UNPAIRED) $@

logs:
	mkdir -p $@

# classifie mate-pairs reads into:
#	real mate-pairs couples:	
#	real paired-ends couples
#	unclassified reads couples
#	garbage reads couples
# by searching for the presence of the linker sequence.
# If the linker is present at the 5' end of one of the two coupled reads, the couple is classified as a mate-pair
# if the linker is present at the 3' end of one of the two coupled reads, the couple is classified as pared-end
# if the linker is not found in any of the two reads, the couple is associated to the unclassified group
# if what remains after linker removal is less then --min-mate, the cauple become garbage
delinked_mate_1.fastq.gz: paired.1.fastq.gz paired.2.fastq.gz logs
	$(call module_loader); \
	nt-linker-sorter-mv \
	--query1 $< \
	--query2 $^2 \
	--dump-garbage \
	--output-prefix $(call first,$(call split ,_,$@)) \
	--linker $(LINKER) \   * Illumina Nextera adapter sequence *
	--gzip \
	| tee $^3/nt-linker-sorter.$@.log


delinked_mate_2.fastq.gz: delinked_mate_1.fastq.gz
	touch $@

delinked_mate_1.fastq: delinked_mate_1.fastq.gz
	zcat $< >$@

delinked_mate_2.fastq: delinked_mate_2.fastq.gz
	zcat $< >$@

# for testing purpose
duplicates_cdhit: delinked_mate_1.fastq delinked_mate_2.fastq
	$(call module_loader); \
	time \
	measure-memory \
	cd-hit-dup -i $< -i2 $^2 -u 25 -o $@

duplicates_sca: delinked_mate_1.fastq delinked_mate_2.fastq
	time \
	measure-memory \
	apriori_duplicates_filtering.pl -1 $< -2 $^2 -p $@

duplicates_fastuniq: delinked_mate_1.fastq delinked_mate_2.fastq
	time \
	measure-memory \
	fastuniq -i <(echo -e "$<\n$^2") -t q -o $@.1 -p $@.2


# for testing purpose
.PHONY: test
test:
	@echo


ALL +=	paired.1.fastq.gz \
	paired.2.fastq.gz \
	delinked_mate_1.fastq.gz


INTERMEDIATE +=


CLEAN += logs \
	 $(wildcard delinked*)