# Copyright 2013 Michele Vidotto <michele.vidotto@gmail.com>

# TODO:

# for testing purpose
.PHONY: test
test:
	@echo

reference.fasta:
	ln -sf $(REFERENCE) $@


logs:
	mkdir -p $@

.PRECIOUS: reference.fasta.bwt
reference.fasta.bwt: logs reference.fasta
	$(call module_loader); \
	bwa index $^2 \
	2>&1 \
	| tee $</bwa.index.$@.log   * [tool].[taget].log *





ALL +=	logs \
	reference.fasta \
	reference.fasta.bwt




INTERMEDIATE += 

CLEAN += logs \
	 $(wildcard reference.fasta.*)



