# Copyright 2013 Michele Vidotto <michele.vidotto@gmail.com>


# path to contaminant file in fasta format
# usually you should use a single fasta containing: chloroplast genome, mitocondrial genome and phiX_added_500bp genome (used for calibration of spike in illumina machine)
CONTAMINANTS ?=

contaminants.fasta:
	ln -sf $(CONTAMINANTS) $@

# prepares database sequences for indexing
contaminants_cleaned.fasta: contaminants.fasta
	cat $< \
	| perl -p -e 's/N\n/N/' \
	| perl -p -e 's/^N+//;s/N+$$//;s/N{200,}/\n>split\n/' \
	| prinseq-lite -log -verbose -fasta stdin -min_len 200 -ns_max_p 10 -derep 12345 -out_good stdout -rm_header -seq_id $* -out_bad null >$@


DECONSEQ_DBS := archDB_s1.tar.gz bactDB_s1.tar.gz bactDB_s2.tar.gz bactDB_s3.tar.gz bactDB_s1.tar.gz bactDB_s2.tar.gz senterica_s1.tar.gz virDB_s1.tar.gz
DECONSEQ_FTP := ftp://edwards.sdsu.edu:7009/deconseq/db/

deconseq_db.info:
	>$@; \
	$(foreach ADDRESS,$(addprefix $(DECONSEQ_FTP),$(DECONSEQ_DBS)),$(call download_deconseqDB,$(ADDRESS),$@)) \   * for each db file name: join ftp and file name, download the database *
	touch $@



# download a single database
define download_deconseqDB
	date | bawk '!/^[$$,\#+]/ { \
	{ print "database_downloaded:",$$0; } \
	}' >> $@; \
	echo -e "$1" >>$@; \
	if [ ! -s $(basename $(basename $(notdir $1))).pac ]; then \
	wget -q -O $(notdir $1) $1; \
	tar -xvzf $(notdir $1); \
	fi;
endef





# for testing purpose
.PHONY: test
test:
	@echo $(FQ1)



ALL +=  contaminants.fasta \
	contaminants.eht


INTERMEDIATE += contaminants_cleaned.fasta


CLEAN += stdin.log



######################################################################
### rules ends here
