# Copyright 2015 Michele Vidotto <michele.vidotto@gmail.com>

LINKER ?= CTGTCTCTTATACACATCT

log:
	mkdir -p $@


1_MATE = $(addsuffix _clean_mate_1.fastq.gz,$(SAMPLES))
log/nt-linker-sorter.%.log.tmp %_clean_garbage_2.fastq.gz %_clean_garbage_1.fastq.gz %_clean_pair_2.fastq.gz %_clean_pair_1.fastq.gz %_clean_unclassified_2.fastq.gz %_clean_unclassified_1.fastq.gz %_clean_mate_2.fastq.gz %_clean_mate_1.fastq.gz: log %.1.fastq.gz %.2.fastq.gz
	$(call load_modules); \
	if [ "$@" == "$*_clean_mate_1.fastq.gz" ]; then \
		nt-linker-sorter \
		--query1 $^2 \
		--query2 $^3 \
		--output-prefix $*_clean \
		--linker $(LINKER) \
		--dump-garbage \
		--min-length 30 \
		--min-mate 30 \
		--gzip \
		2>&1 \
		| tee $</nt-linker-sorter.$*.log.tmp; \
	else \
		until [ -s $*_clean_mate_1.fastq.gz ]; do \   * wait for clean_mate_1.fastq.gz to be ready *
			sleep 15; \
			echo "process for sequence $@ is spleeping 15 seconds.."; \
		done; \
	fi

2_MATE = $(addsuffix _clean_mate_2.fastq.gz,$(SAMPLES))
# %_clean_mate_2.fastq.gz: %_clean_mate_1.fastq.gz
# 	touch $@

1_UNCLASS = $(addsuffix _clean_unclassified_1.fastq.gz,$(SAMPLES))
# %_clean_unclassified_1.fastq.gz: %_clean_mate_1.fastq.gz
# 	touch $@

2_UNCLASS = $(addsuffix _clean_unclassified_2.fastq.gz,$(SAMPLES))
# %_clean_unclassified_2.fastq.gz: %_clean_mate_1.fastq.gz
# 	touch $@

1_PAIR = $(addsuffix _clean_pair_1.fastq.gz,$(SAMPLES))
# %_clean_pair_1.fastq.gz: %_clean_mate_1.fastq.gz
# 	touch $@

2_PAIR = $(addsuffix _clean_pair_2.fastq.gz,$(SAMPLES))
# %_clean_pair_2.fastq.gz: %_clean_mate_1.fastq.gz
# 	touch $@

1_GARBAGE = $(addsuffix _clean_garbage_1.fastq.gz,$(SAMPLES))
# %_clean_garbage_1.fastq.gz: %_clean_mate_1.fastq.gz
# 	touch $@

2_GARBAGE = $(addsuffix _clean_garbage_2.fastq.gz,$(SAMPLES))
# %_clean_garbage_2.fastq.gz: %_clean_mate_1.fastq.gz
# 	touch $@

NT_LS_LOG_TMP = $(addprefix log/nt-linker-sorter.,$(addsuffix .log.tmp,$(SAMPLES)))

.META: nt-linker-sorter.%.log
	1	sample
	2	garbage
	3	mate
	4	pair
	5	unclassified
	6	double putative pair
	7	double unknown pair
	8	no alignments
	9	not trailing alignments below 16
	10	not justapoxed
	11	short after Ns removal
	12	too many alignments

NT_LS_LOG = $(addprefix nt-linker-sorter.,$(addsuffix .log,$(SAMPLES)))

nt-linker-sorter.%.log: log/nt-linker-sorter.%.log.tmp
	paste -d "\t" \
	<(bawk '/^File statistics:/{flag=1;next}/^$$/{flag=0}flag { \
		split($$0, a, "\t"); \
		print a[2]; }' $< \
	| transpose) \
	<(bawk '/^Other statistics:/{flag=1;next}/^$$/{flag=0}flag { \
		split($$0, a, "\t"); \
		print a[2]; }' $< \
	| transpose) \
	| sed 's/^/$*\t/' >$@




define revcomp
	$(call load_modules); \
	zcat $2 | fastx_reverse_complement -z -v -Q33 2>$3 3>&1 1>&2 2>&3 \
	| tee $1/fastx_reverse_complement.$3.log
endef


1_MATE_REVCOMP = $(addsuffix _clean_mate_revcomp_1.fastq.gz,$(SAMPLES))
%_clean_mate_revcomp_1.fastq.gz: log %_clean_mate_1.fastq.gz
	$(call revcomp,$<,$^2,$@)

2_MATE_REVCOMP = $(addsuffix _clean_mate_revcomp_2.fastq.gz,$(SAMPLES))
%_clean_mate_revcomp_2.fastq.gz: log %_clean_mate_2.fastq.gz
	$(call revcomp,$<,$^2,$@)

1_UNCLASS_REVCOMP = $(addsuffix _clean_unclassified_revcomp_1.fastq.gz,$(SAMPLES))
%_clean_unclassified_revcomp_1.fastq.gz: log %_clean_unclassified_1.fastq.gz
	$(call revcomp,$<,$^2,$@)

2_UNCLASS_REVCOMP = $(addsuffix _clean_unclassified_revcomp_2.fastq.gz,$(SAMPLES))
%_clean_unclassified_revcomp_2.fastq.gz: log %_clean_unclassified_2.fastq.gz
	$(call revcomp,$<,$^2,$@)



.PHONY: test
test:
	@echo


ALL +=	log \
	$(1_CLEAN_GZ) $(2_CLEAN_GZ) \
	$(1_MATE) $(2_MATE) \
	$(1_UNCLASS) $(2_UNCLASS) \
	$(1_PAIR) $(2_PAIR) \
	$(1_GARBAGE) $(2_GARBAGE) \
	$(1_MATE_REVCOMP) $(2_MATE_REVCOMP) \
	$(1_UNCLASS_REVCOMP) $(2_UNCLASS_REVCOMP) \
	$(NT_LS_LOG)


INTERMEDIATE += $(NT_LS_LOG_TMP)

CLEAN += 
