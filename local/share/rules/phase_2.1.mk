# Copyright 2015 Michele Vidotto <michele.vidotto@gmail.com>

extern ../phase_1/contaminants.eht as CONTAMINANT_INDEXES

MIN_OVERLAP_LEN ?= 10
MIN_SIZE ?= 35

log:
	mkdir -p $@

contaminants.eht:
	ln -sf $(CONTAMINANT_INDEXES) $@


# this function install all the links at once
1_FASTQ_GZ = $(addsuffix .1.fastq.gz,$(SAMPLES))
2_FASTQ_GZ = $(addsuffix .2.fastq.gz,$(SAMPLES))
3_FASTQ_GZ = $(addsuffix .unpaired.fastq.gz,$(SAMPLES))
AD = $(addsuffix .adapters,$(SAMPLES))

%.adapters %.1.fastq.gz %.2.fastq.gz %.unpaired.fastq.gz:
	@echo installing links ... \
	$(foreach SAMPLE,$(SAMPLES), \
		$(shell ln -sf ../phase_2/$(SAMPLE).cutadapt.erne_1.fastq.gz $(SAMPLE).1.fastq.gz) \
		$(shell ln -sf ../phase_2/$(SAMPLE).cutadapt.erne_2.fastq.gz $(SAMPLE).2.fastq.gz) \
		$(shell ln -sf ../phase_2/$(SAMPLE).cutadapt.erne_unpaired.fastq.gz $(SAMPLE).unpaired.fastq.gz) \
		$(shell echo -e '$(call get,$(SAMPLE),AD1)\t$(call get,$(SAMPLE),AD2)' >$(SAMPLE).adapters) \
	) \
	&& sleep 3


# [PIPELINE]
# name: Trimming_PE_new_order
# codename: trimming-pe-new-order
# code: 0b9d3bac-d7a7-43a0-9917-6b87ed7fa024
# [step.s1]
# label: cutadapt1
define cutadapt
	$(call load_modules); \
	cutadapt \
	--anywhere "$$(cat $2 | cut -f1)" \
	--anywhere "$$(cat $2 | cut -f2)" \
	--format fastq \
	--overlap $(MIN_OVERLAP_LEN) \
	--times 2 \
	--mask-adapter \
	--output $4 \
	$3 \
	| tee $1/cutadapt.$4.log.tmp
endef


1_CUTADAPT = $(addsuffix .1.cutadapt.gz,$(SAMPLES))
%.1.cutadapt.gz: log %.adapters %.1.fastq.gz
	$(call cutadapt,$<,$^2,$^3,$@)

2_CUTADAPT = $(addsuffix .2.cutadapt.gz,$(SAMPLES))
%.2.cutadapt.gz: log %.adapters %.2.fastq.gz
	$(call cutadapt,$<,$^2,$^3,$@)

# the unpaired reads jast need a cutadapt run, not erne filter
3_CUTADAPT = $(addsuffix .unpaired.cutadapt.gz,$(SAMPLES))
%.unpaired.cutadapt.gz: log %.adapters %.unpaired.fastq.gz
	$(call load_modules); \
	cutadapt \
	--anywhere "$$(cat $^2 | cut -f1)" \
	--anywhere "$$(cat $^2 | cut -f2)" \
	--format fastq \
	--overlap $(MIN_OVERLAP_LEN) \
	--times 2 \
	--minimum-length $(MIN_SIZE) \
	--output $@ \
	$^3 \
	| tee $</cutadapt.$@.log.tmp


1_CUTADAPT_LOG_TMP = $(addprefix log/cutadapt.,$(addsuffix .1.cutadapt.gz.log.tmp,$(SAMPLES)))
log/cutadapt.%.1.cutadapt.gz.log.tmp: %.1.cutadapt.gz
	touch $@

2_CUTADAPT_LOG_TMP = $(addprefix log/cutadapt.,$(addsuffix .2.cutadapt.gz.log.tmp,$(SAMPLES)))
log/cutadapt.%.2.cutadapt.gz.log.tmp: %.2.cutadapt.gz
	touch $@

3_CUTADAPT_LOG_TMP = $(addprefix log/cutadapt.,$(addsuffix .unpaired.cutadapt.gz.log.tmp,$(SAMPLES)))
log/cutadapt.%.unpaired.cutadapt.gz.log.tmp: %.unpaired.cutadapt.gz
	touch $@


.META: cutadapt.%.log 2.cutadapt.%.log
	1	sample
	2	no. of adapters
	3	processed reads
	4	processed bases
	5	trimmed reads
	6	trimmed bases
	7	too short reads
	8	too long reads
	9	total time
	10	time per read
	11	adapter 1
	12	num. Trim
	13	found at 5 end
	14	found at 3 end or middle
	15	adapter 2
	16	num. Trim
	17	found at 5 end
	18	found at 3 end or middle


define cutadapt_log
	>$2; \
	for FILE in $1; do \
		TMP=$$(basename "$$FILE"); \
		TMP1=$${TMP%$3}; \
		paste -d "\t" \
		<(bawk '/^[ +]/ { \
			print $$0; }' <$$FILE \
		| tr -s " " " " \
		|  bawk '!/^[\#+,$$]/ { \
			split($$0, a, ":"); \
			split(a[2], b, " "); \
			gsub(" ", "", a[2]); \
			print b[1]; }' \
		| transpose) \
		\
		<(bawk '/^Adapter/,/^$$/ { \
			print $$0; }' <$$FILE \
		| tr -d "'" \
		| tr \\n " " \
		| bawk '!/^[\#+,$$]/ { \
			gsub(",", "", $$0); \
			split($$0, a, " "); \
			print a[2], a[7], a[9], a[19], a[32], a[37], a[39], a[49];}') \
		| sed "s/^/$$TMP1\t/" >>$2; \
	done
endef

CUTADAPT_LOG = $(addprefix cutadapt.,$(addsuffix .log,$(SAMPLES)))
cutadapt.%.log: log/cutadapt.%.1.cutadapt.gz.log.tmp log/cutadapt.%.2.cutadapt.gz.log.tmp log/cutadapt.%.unpaired.cutadapt.gz.log.tmp
	$(call cutadapt_log,$^,$@,.cutadapt.gz.log.tmp)



# [PIPELINE]
# name: Trimming_PE_new_order
# codename: trimming-pe-new-order
# code: 0b9d3bac-d7a7-43a0-9917-6b87ed7fa024
# [step.s4]
# label: erne-filter
#	--contamination-reference $^4
#	--no-indels
1_ERNE = $(addprefix tmp.,$(addsuffix .cutadapt.erne_1.fastq.gz,$(SAMPLES)))
tmp.%.cutadapt.erne_1.fastq.gz: log %.1.cutadapt.gz %.2.cutadapt.gz contaminants.eht
	!threads
	$(call load_modules); \
	erne-filter \
	--query1 $^2 \
	--query2 $^3 \
	--min-size $(MIN_SIZE) \
	--threads $$THREADNUM \
	--gzip \
	--output-prefix "tmp.$*.cutadapt.erne" \
	| tee $</erne-filter.$*.log.tmp


2_ERNE = $(addprefix tmp.,$(addsuffix .cutadapt.erne_2.fastq.gz,$(SAMPLES)))
3_ERNE = $(addprefix tmp.,$(addsuffix .cutadapt.erne_unpaired.fastq.gz,$(SAMPLES)))
tmp.%.cutadapt.erne_unpaired.fastq.gz tmp.%.cutadapt.erne_2.fastq.gz: tmp.%.cutadapt.erne_1.fastq.gz
	touch $@

ERNE_LOG_TMP = $(addprefix log/erne-filter.,$(addsuffix .log.tmp,$(SAMPLES)))
log/erne-filter.%.log.tmp: tmp.%.cutadapt.erne_1.fastq.gz
	touch $@

.META: erne-filter.%.log
	1	sample
	2	reads
	3	good reads
	4	discarded reads
	5	contaminated reads
	6	good bases

ERNE_LOG = $(addprefix erne-filter.,$(addsuffix .log,$(SAMPLES)))
erne-filter.%.log: log/erne-filter.%.log.tmp
	bawk '/^\# / { \
		split($$0, a, ":"); \
		gsub(" ", "", $$0); \
		print a[2]; }' $< \
	| transpose \
	| sed 's/^/$*\t/' >$@

1_FINAL = $(addsuffix .cutadapt.erne_1.fastq.gz,$(SAMPLES))
%.cutadapt.erne_1.fastq.gz: tmp.%.cutadapt.erne_1.fastq.gz
	mv $< $@

2_FINAL = $(addsuffix .cutadapt.erne_2.fastq.gz,$(SAMPLES))
%.cutadapt.erne_2.fastq.gz: tmp.%.cutadapt.erne_2.fastq.gz
	mv $< $@

3_FINAL = $(addsuffix .cutadapt.erne_unpaired.fastq.gz,$(SAMPLES))
%.cutadapt.erne_unpaired.fastq.gz: tmp.%.cutadapt.erne_unpaired.fastq.gz %.unpaired.cutadapt.gz
	zcat $^ \
	| gzip -c >$@


phase.end.flag: $(1_FINAL) $(2_FINAL) $(3_FINAL) $(ERNE_LOG)
	touch $@

import move_file

.PHONY: test
test:
	@echo



ALL += log \
	$(1_FASTQ_GZ) \
	$(2_FASTQ_GZ) \
	$(3_FASTQ_GZ) \
	$(AD) \
	$(CUTADAPT_LOG) \
	$(ERNE_LOG) \
	$(1_FINAL) $(2_FINAL) $(3_FINAL) \
	phase.end.flag

INTERMEDIATE += $(1_CUTADAPT) $(2_CUTADAPT) $(3_CUTADAPT) \
	$(1_ERNE) $(2_ERNE) $(3_ERNE)

CLEAN += contaminants.eht
