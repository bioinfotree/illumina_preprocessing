# Copyright 2013 Michele Vidotto <michele.vidotto@gmail.com>


# path to contaminant file in fasta format
# usually you should use a single fasta containing: chloroplast genome, mitocondrial genome and phiX_added_500bp genome (used for calibration of spike in illumina machine)
CONTAMINANTS ?=

CONTAMINANTS_EHT ?=

# kmer length used by bbduk
BBDUK_KMERS ?= 27
# reads with more than this many contaminant kmers for bbduk
MAXBADKMERS ?= 1
# maximum edit distance from ref kmers for bbduk
EDIT_DISTANCE ?= 0

log:
	mkdir -p $@

contaminants.fasta:
	ln -sf $(CONTAMINANTS) $@


# this function install all the links at once
1_FASTQ_GZ = $(addsuffix .1.fastq.gz,$(SAMPLES))
2_FASTQ_GZ = $(addsuffix .2.fastq.gz,$(SAMPLES))

%.1.fastq.gz %.2.fastq.gz:
	@echo installing links ... \
	$(foreach SAMPLE,$(SAMPLES), \
		$(shell ln -sf $(call get,$(SAMPLE),1) $(SAMPLE).1.fastq.gz) \
		$(shell ln -sf $(call get,$(SAMPLE),2) $(SAMPLE).2.fastq.gz) \
	) \
	&& sleep 3


# prepares database sequences for indexing
contaminants_cleaned.fasta: log contaminants.fasta
	$(call load_modules); \
	cat $^2 \
	| perl -p -e 's/N\n/N/' \
	| perl -p -e 's/^N+//;s/N+$$//;s/N{200,}/\n>split\n/' \
	| prinseq-lite.pl \
	-log $</prinseq-lite.$@.log \
	-verbose \
	-fasta stdin \
	-min_len 200 \
	-ns_max_p 10 \
	-derep 12345 \
	-out_good stdout \
	-rm_header \
	-seq_id $* \
	-out_bad null >$@


1_DC = $(addsuffix .1.dc.fastq.gz,$(SAMPLES))

# filter contaminants using bbduk
%.1.dc.fastq.gz: log contaminants_cleaned.fasta %.1.fastq.gz %.2.fastq.gz
	!threads
	$(call load_modules); \
	bbduk2.sh \
	in=$^3 \
	in2=$^4 \
	fref=$^2 \
	out=$@ \
	out2=$*.2.dc.fastq.gz \
	outm=$*.1.ct.fastq.gz \
	outm2=$*.2.ct.fastq.gz \
	stats=$</bbduk2.$*.stats.tmp \
	overwrite=true \
	threads=$$THREADNUM \
	editdistance=$(EDIT_DISTANCE) \
	maxbadkmers=$(MAXBADKMERS) \
	k=$(BBDUK_KMERS) \
	2>&1 \
	| tee $</bbduk2.$*.log


2_DC = $(addsuffix .2.dc.fastq.gz,$(SAMPLES))
1_CT = $(addsuffix .1.ct.fastq.gz,$(SAMPLES))
2_CT = $(addsuffix .2.ct.fastq.gz,$(SAMPLES))

%.2.ct.fastq.gz %.1.ct.fastq.gz %.2.dc.fastq.gz: %.1.dc.fastq.gz
	touch $@

BBDUK_LOG_TMP = $(addsuffix .stats.tmp, $(addprefix log/bbduk2.,$(SAMPLES)))
log/bbduk2.%.stats.tmp: %.1.dc.fastq.gz
	touch $@

BBDUK_LOG = $(addsuffix .stats, $(addprefix bbduk2.,$(SAMPLES)))

.META: bbduk2.%.stats
	1	sample
	2	total sequences #
	3	contaminated sequences #
	4	contaminated sequences %
	5	contaminated R1 #
	6	contaminated R1 %
	7	contaminated R2 #
	8	contaminated R2 %



bbduk2.%.stats: log/bbduk2.%.stats.tmp
	>$@; \
	for FILE in $^; do \
		tr \\n \\t <$< \
		| select_columns 5 7 8 13 14 16 17 \
		| sed 's/^/$*\t/' >$@; \
	done


# indexing contamint file for erne-filter
ifndef CONTAMINANTS_EHT

contaminants.eht: log contaminants_cleaned.fasta
	$(call load_modules); \
	erne-create --fasta $^2 --reference-prefix $(basename $@) \
	2>&1 \
	| tee $</erne-create.$@.log

else

contaminants.eht:
	ln -sf $(CONTAMINANTS_EHT) $@

endif


# for testing purpose
.PHONY: test
test:
	@echo $(FQ1)



ALL +=	log \
	contaminants.fasta \
	$(1_FASTQ_GZ) \
	$(2_FASTQ_GZ) \
	$(1_DC) $(2_DC) \
	$(1_CT) $(2_CT) \
	contaminants.eht \
	$(BBDUK_LOG)

INTERMEDIATE += contaminants_cleaned.fasta

CLEAN += $(1_CT) $(2_CT)