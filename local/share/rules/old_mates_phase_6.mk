# Copyright 2013 Michele Vidotto <michele.vidotto@gmail.com>

# TODO:
#	optimization bwa mem
#
#	why was used samtools import for target same.chr.bam

# @RG Read group. Unordered multiple @RG lines are allowed.
# Read group identier. Each @RG line must have a unique ID. The value of ID is used in the RG
# 	tags of alignment records. Must be unique among all read groups in hRead group_IDeader section. Read group
# 	IDs may be modied when merging SAM les in order to handle collisions.
READ_GROUP_ID ?= 001
# 	CN Name of sequencing center producing the read.
READ_GROUP_CN ?= IGA
# 	DS Description.
READ_GROUP_DS ?= TRAMINER
# 	DT Date the run was produced (ISO8601 date or date/time).
READ_GROUP_DT ?= 1997-07-16T19:20+01:00
# 	FO Flow order. The array of nucleotide bases that correspond to the nucleotides used for each
# 	ow of each read. Multi-base ows are encoded in IUPAC format, and non-nucleotide ows by
# 	various other characters. Format: /\*|[ACMGRSVTWYHKDBN]+/
READ_GROUP_FO ?= 1
# 	KS The array of nucleotide bases that correspond to the key sequence of each read.
READ_GROUP_KS ?= 1
# 	LB Library.
READ_GROUP_LB ?= TRAMINER
# 	PG Programs used for processing the read group.
READ_GROUP_PG ?= BWA-MEM
# 	PI Predicted median insert size.
READ_GROUP_PI ?= 0
# 	PL Platform/technology used to produce the reads. Valid values: CAPILLARY, LS454, ILLUMINA, SOLID, HELICOS,IONTORRENT and PACBIO.
READ_GROUP_PL ?= ILLUMINA
# 	PU Platform unit (e.g. owcell-barcode.lane for Illumina or slide for SOLiD). Unique identifer.
READ_GROUP_PU ?= 001
# 	SM Sample. Use pool name where a pool is being sequenced
READ_GROUP_SM ?= TRAMINER


extern ../phase_4/delinked_mate_1.fastq.gz		as MATE1
extern ../phase_4/delinked_mate_2.fastq.gz		as MATE2
extern ../phase_4/delinked_pair_1.fastq.gz		as PAIRED1
extern ../phase_4/delinked_pair_2.fastq.gz		as PAIRED2
extern ../phase_4/delinked_unclassified_1.fastq.gz	as UNCLASSIFIED1
extern ../phase_4/delinked_unclassified_2.fastq.gz	as UNCLASSIFIED2

extern ../phase_5/reference.fasta.*			as INDEX

# for testing purpose
.PHONY: test
test:
	@echo

reference.fasta:
	ln -sf $(REFERENCE) $@

mate.1.fastq.gz:
	ln -sf $(MATE1) $@

mate.2.fastq.gz:
	ln -sf $(MATE2) $@

paired_end.1.fastq.gz:
	ln -sf $(PAIRED1) $@

paired_end.2.fastq.gz:
	ln -sf $(PAIRED2) $@

unclassified.1.fastq.gz:
	ln -sf $(UNCLASSIFIED1) $@

unclassified.2.fastq.gz:
	ln -sf $(UNCLASSIFIED2) $@

logs:
	mkdir -p $@


reference.fasta.bwt:
	$(call module_loader); \
	for FILE in $(INDEX); do \
	   ln -sf $$FILE $$(basename $$FILE); \
	done



%.fastq: %.fastq.gz
	zcat $< >$@


# align paired reads
%.sam: reference.fasta.bwt %.1.fastq %.2.fastq logs
	!threads
	$(call module_loader); \
	bwa mem \
	-t $$THREADNUM \   * number of threads *
	-M \   * Mark shorter split hits as secondary (for Picard compatibility) *
	-R "@RG\tID:$(READ_GROUP_ID)\tCN:$(READ_GROUP_CN)\tDS:$(READ_GROUP_DS)\tDT:$(READ_GROUP_DT)\tFO:$(READ_GROUP_FO)\tKS:$(READ_GROUP_KS)\tLB:$(READ_GROUP_LB)\tPG:$(READ_GROUP_PG)\tPI:$(READ_GROUP_PI)\tPL:$(READ_GROUP_PL)\tPU:$(READ_GROUP_PU)\tSM:$(READ_GROUP_SM)" \   * Complete read group header line. *
	$(basename $<) \   * db prefix *
	$^2 \   * reads 1 *
	$^3 \   * reads 2 *
	2>$@ 3>&1 1>&2 2>&3 \   * redirect stdout to file, stderr to stdout and file *
	| tee $^4/bwa-mem.$@.log   * [tool].[taget].log *


# converts sam to bam
define sam2bam
$(call module_loader); \
samtools view \
-bS \
$1 \
2>$2/samtools-view.$@.log \
| samtools sort -@ $$THREADNUM - $(basename $3)   * with -o option samtools sort need out file prefix the same *
endef

# converts sam to bam and sort
# each targhet must be specified singly otherwise bmake get into circular reference
# and returns error!
paired_end.bam: paired_end.sam logs
	!threads
	$(call sam2bam,$<,$^2,$@)

mate.bam: mate.sam logs
	!threads
	$(call sam2bam,$<,$^2,$@)
	

unclassified.bam: unclassified.sam logs
	!threads
	$(call sam2bam,$<,$^2,$@)


# it seems that samtools fixmate determine an error when used with mate pairs
#	| samtools sort -o -@ $$THREADNUM - $(basename $@) \   * with -o option samtools sort need out file prefix the same *
#	| samtools fixmate - $@   * because samtools rmdup works better when the insert size is set correctly, samtools fixmate can be run to fill in mate coordinates, ISIZE and mate related flags *


# merge all bam files. Use Picard instead of samtools merge, because samtools merge produces a bam file
# that give error with CollectMultipleMetrics.jar
mate_unclassified.bam: mate.bam unclassified.bam logs
	$(call module_loader); \
	java_exec MergeSamFiles.jar \
	USE_THREADING=true \
	INPUT=$< \
	INPUT=$^2 \
	OUTPUT=$@ \
	VALIDATION_STRINGENCY=LENIENT \
	3>&1 1>&2 2>&3 \
	| tee $^3/picard-MergeSamFiles.$@.log


# remove PCR duplicates
nodup.%.bam: %.bam logs
	!threads
	$(call module_loader); \
	samtools rmdup -s $< - 2>$^2/samtools-rmdup.$@.log \
	| samtools sort -@ $$THREADNUM - $(basename $@)



# collect metrics of alignment by recalling other tools 

# CollectInsertSizeMetrics		calculate metrics about the statistical distribution of insert size (excluding duplicates) and generates a histogram plot.
# CollectAlignmentSummaryMetrics:	calculate summary alignment metrics 
# QualityScoreDistribution:		chart quality score distributions 
# MeanQualityByCycle:			calculate mean quality at every cicle

# PIRED-END	INWARD	FORWARD-REVERSE
# A---------------------------------B
# ----->                       <-----
# read 1                       read 2

# MATE-PAIRS	OUTWARD	REVERSE-FORWARD
# A---------------------------------B
# <-----                      ------>
# read 1                       read 2
%_summary_metrics: reference.fasta %.bam logs
	$(call module_loader); \
	java_exec CollectMultipleMetrics.jar \
	REFERENCE_SEQUENCE=$< \
	INPUT=$^2 \
	OUTPUT=$(basename $@) \
	VALIDATION_STRINGENCY=LENIENT \
	ASSUME_SORTED=false \
	PROGRAM=CollectAlignmentSummaryMetrics \
	PROGRAM=QualityScoreDistribution \
	PROGRAM=MeanQualityByCycle \
	PROGRAM=CollectInsertSizeMetrics \
	3>&1 1>&2 2>&3 \
	| tee $^3/picard-CollectMultipleMetrics.$@.log


# select alignments with mapping quality more then 20 and that are primary

# The correct placement of a read may be ambiguous, e.g. due to repeats. In
# this case, there may be multiple read alignments for the same read. One of these alignments is
# considered primary. All the other alignments have the secondary alignment ag set in the SAM
# records that represent them.
uniq.nodup.%.bam: nodup.%.bam
	$(call module_loader); \
	samtools view \
	-bhq 20 \
	-F '0x0100' \   * skip alignment that are not primary *
	$< >$@


# select only reads that maps to same chr in order to prevent problems with Breakdancer (caller for deletions).
# Reads that map in different chrs are not usefull for the identification of deletions with Breakdancer
# The 7th field of sam/bam indicate the reference name on which the paired sequence align.
# If it is the same as for the first reads this field became ‘=’ 
same.chr.uniq.nodup.%.bam: uniq.nodup.%.bam
	$(call module_loader); \
	samtools view -h $< \
	| bawk '!/^[$$,\#+]/ { if (  $$1 ~ /^@/  \   * in order to include header *
	|| $$7 == "=" ) print $$0; }' \
	| samtools view -bhS - >$@

 
# selection only reads that are mapped in a proper pair. See bwa man.
prop_paired.uniq.nodup.%.bam: uniq.nodup.%.bam
	$(call module_loader); \
	samtools view -f '0x0002' -hb $< >$@


# calcolate genome size, genome really coverad and coverage of genome really covered
%.cov: reference.fasta %.bam
	!serial
	$(call module_loader); \
	coverage -f $< $^2 >$@


.PHONY: test
test:
	@echo $(INDEX)

ALL +=	logs \
	reference.fasta \
	reference.fasta.bwt \
	\
	mate.1.fastq.gz \
	mate.2.fastq.gz \
	paired_end.1.fastq.gz \
	paired_end.2.fastq.gz \
	unclassified.1.fastq.gz \
	unclassified.2.fastq.gz \
	\
	paired_end.bam \
	mate.bam \
	unclassified.bam \
	mate_unclassified.bam \
	\
	nodup.mate_unclassified.bam \
	nodup.paired_end.bam \
	uniq.nodup.mate_unclassified.bam \
	uniq.nodup.paired_end.bam \
	same.chr.uniq.nodup.mate_unclassified.bam \
	same.chr.uniq.nodup.paired_end.bam \
	prop_paired.uniq.nodup.mate_unclassified.bam \
	prop_paired.uniq.nodup.paired_end.bam \
	\
	mate_unclassified_summary_metrics \
	nodup.mate_unclassified_summary_metrics \
	uniq.nodup.mate_unclassified_summary_metrics \
	same.chr.uniq.nodup.mate_unclassified_summary_metrics \
	prop_paired.uniq.nodup.mate_unclassified_summary_metrics \
	\
	paired_end_summary_metrics \
	nodup.paired_end_summary_metrics \
	uniq.nodup.paired_end_summary_metrics \
	same.chr.uniq.nodup.paired_end_summary_metrics \
	prop_paired.uniq.nodup.paired_end_summary_metrics \
	\
	mate_summary_metrics \
	unclassified_summary_metrics \
	\
	mate_unclassified.cov \
	nodup.mate_unclassified.cov \
	uniq.nodup.mate_unclassified.cov \
	same.chr.uniq.nodup.mate_unclassified.cov \
	prop_paired.uniq.nodup.mate_unclassified.cov \
	\
	paired_end.cov \
	nodup.paired_end.cov \
	uniq.nodup.paired_end.cov \
	same.chr.uniq.nodup.paired_end.cov \
	prop_paired.uniq.nodup.paired_end.cov




INTERMEDIATE += paired_end.1.fastq \
		paired_end.2.fastq \
		mate.1.fastq \
		mate.2.fastq \
		unclassified.1.fastq \
		unclassified.2.fastq \
		paired_end.sam \
		mate.sam \
		unclassified.sam


CLEAN += logs \
	 $(wildcard reference.fasta.*) \
	 $(wildcard *.pdf) \
	 $(wildcard *_metrics) \




