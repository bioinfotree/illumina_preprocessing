#!/usr/bin/env python

'''
This script removes uncleaned reads from single read/paired ends reads cleaned by cutadapt.
Sometimes happens that cutadapt removes 3' adapter from 5' but consider it as a 5' adapter,
leaving the read with a lot of N followed by polyA (at least five).
Those reads must be masked from both R1 and R2.
The check is performed on R1's 5p.

Authors: Zamboni
Contact: zamboni@appliedgenomics.org
Date of creation: 28-04-14
Version: 1.0.1
'''

import argparse
import logging
import subprocess
import sys

logging.basicConfig()
logger = logging.getLogger('Clean removed adapter')

from pysam import FastqFile


def fastq_read_to_string(name, comment, sequence, quality):
    return '@%s %s\n%s\n+\n%s\n' % (name, comment, sequence, quality)


def clean_removed_adapter(r1_path, **kwargs):
    """Clean reads starting with N*A*.
        Arguments:
            - r1_path: path of R1
        Kwargs:
            - r2_path: path of R2
            - output_suffix: suffix to append to path names for cleaned files.
            - min_n: minimum number of N at 5p
            - min_a: minimum number of A following 5p's N
    """
    output_suffix = kwargs.get('output_suffix', 'cleaned')
    r2_path = kwargs.get('r2_path', '')
    min_n = kwargs.get('min_n', 20)
    min_a = kwargs.get('min_a', 5)

    r1 = FastqFile(r1_path)
    r1_output_path = output_suffix + '_1.fastq'
    r1_output_h = open(r1_output_path, 'w+')

    if r2_path:
        r2 = FastqFile(r2_path)
        r2_output_path = output_suffix + '_2.fastq'
        r2_output_h = open(r2_output_path, 'w+')

    n_string = 'N' * min_n
    a_string = 'A' * min_a

    parsed_reads = 0
    cleaned_reads = 0
    for read in r1:
        clean_read = True  # read is already clean
        parsed_reads += 1
        if parsed_reads % 1000000 == 0:
            logger.info('Parsed %s reads' % parsed_reads)
        if read.sequence.startswith(n_string):
            sequence = read.sequence.lstrip('N')
            if sequence.startswith(a_string):
                cleaned_reads += 1
                clean_read = False
                if cleaned_reads % 100 == 0:
                    logger.info('\t%s N*A* reads discovered' % cleaned_reads)
                new_sequence = 'N' * len(read.quality)
                r1_read_string = fastq_read_to_string(read.name, read.comment,
                                                      new_sequence, read.quality)
                r1_output_h.write(r1_read_string)
                if r2_path:
                    read_2 = next(r2)
                    new_sequence = 'N' * len(read_2.quality)
                    r2_read_string = fastq_read_to_string(read_2.name, read_2.comment,
                                                          new_sequence, read_2.quality)
                    r2_output_h.write(r2_read_string)
        # if read is clan, write it as-is
        if clean_read:
            r1_read_string = fastq_read_to_string(read.name, read.comment,
                                                  read.sequence, read.quality)
            r1_output_h.write(r1_read_string)
            if r2_path:
                read_2 = next(r2)
                r2_read_string = fastq_read_to_string(read_2.name, read_2.comment,
                                                      read_2.sequence, read_2.quality)
                r2_output_h.write(r2_read_string)

    logger.info('--> %s N*A* reads discovered' % cleaned_reads)

    r1_output_h.close()
    if r2_path:
        r2_output_h.close()

    logger.info('Compress R1: %s' % r1_output_path)
    try:
        subprocess.check_call(['gzip', '-f', r1_output_path])
    except subprocess.CalledProcessError as e:
        sys.stderr.write("Error during gizp command\n%s" % e)

    if r2_path:
        logger.info('Compress R2: %s' % r2_output_path)
        try:
            subprocess.check_call(['gzip', '-f', r2_output_path])
        except subprocess.CalledProcessError as e:
            sys.stderr.write("Error during gizp command\n%s" % e)


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
            prog="clean_removed_adapter",
            description='Clean reads starting with N*A*')
    parser.add_argument("-1", "--R1",
                        dest="r1_path",
                        required=True,
                        help="R1 path")
    parser.add_argument("-2", "--R2",
                        dest="r2_path",
                        default='',
                        help="R2 path (optional)")
    parser.add_argument('-o', '--output-suffix',
                        dest="output_suffix",
                        default='cleaned',
                        help="Output suffix used to create cleaned reads (default 'cleaned')")
    parser.add_argument('-n', '--min-n',
                        dest="min_n",
                        default=20,
                        type=int,
                        help="Minimum number of N at 5p (default: 20)")
    parser.add_argument('-a', '--min-a',
                        dest="min_a",
                        default=5,
                        type=int,
                        help="Minimum number of A that follow 5p's N (default: 5)")
    parser.add_argument("-v", "--verbose",
                        action="store_true",
                        dest="verbose",
                        default=False,
                        help="Verbose actions")
    parser.add_argument('--version',
                        action='version',
                        version='%(prog)s 1.0.1')

    args = parser.parse_args()

    if args.verbose:
        logger.setLevel(logging.INFO)

    clean_removed_adapter(args.r1_path, r2_path=args.r2_path,
                          output_suffix=args.output_suffix,
                          min_n=args.min_n, min_a=args.min_a)
