#!/bin/bash
# Copyright 2013 Gabriele Magris <gmagris@appliedgenomics.org>
# Copyright 2013 Michele Vidotto <michele.vidotto@gmail.com>

# Script to generate WIG file from BAM and calculate the coverage

usage()
{
cat << EOF
Usage: ${0##/*} [OPTIONS] -f FASTA [[BAM]..]

Generate WIG file and calculate Genome Size, Genome Covered and Sum of Coverage
for every BAM alignment file


OPTIONS:
   -h      Show this message
   -f      reference file in FASTA format
   -r      after calculation remove XXXXX.tmp DIR containing gzip compressed WIG file
EOF
}

REF=
ALN=
RM_TMP=

while getopts "hrf:" OPTION
do
     case $OPTION in
         h)
             usage
             exit 1
             ;;
         f)
             REF=$OPTARG
             ;;
         r)
             RM_TMP=1
             ;;
         ?)
             usage
             exit
             ;;
     esac
done


if [[ -z $REF ]]
then
     usage
     exit 1
fi

# remove arguments taken by getopt
shift $(($OPTIND - 1))

# test if at least one positional argumet is present
if [ -z $1 ]
then
	echo "ERROR ${0##/*}: Wrong argument number"
	usage
	exit 1
fi

# generate tmp dir
TEMP_DIR=$(mktemp -d -q $(basename "${1%.*}").XXXXXX)


for ALN in $*; do
	FILE_BASENAME=$(basename "$ALN")
	WIG="${FILE_BASENAME%.*}.wig"
	# compute WIG file from BAM alignment
	compute_profile --input-file $ALN --sam-format --fasta $REF --output-file $TEMP_DIR/$WIG 1>&2 # redirect STDOUT to STDERR
	fasta_length <$REF >$TEMP_DIR/${FILE_BASENAME%.*}.chrom.sizes
	wigToBigWig $TEMP_DIR/$WIG $TEMP_DIR/${FILE_BASENAME%.*}.chrom.sizes $TEMP_DIR/${FILE_BASENAME%.*}.bw
	# calcolate coverage from WIG file. Results to STDOUT
	# coverage_WIG.pl must be present in the same DIR of this script
	$(dirname "$(readlink -sf $0)")/coverage_WIG.pl --WIG $TEMP_DIR/$WIG
	if [ -z "${RM_TMP}" ]; then
		gzip $TEMP_DIR/$WIG
	fi
done

if [ -n "${RM_TMP}" ]; then
	rm -r $TEMP_DIR
fi

exit 0
