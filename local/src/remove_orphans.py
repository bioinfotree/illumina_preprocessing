#!/usr/bin/env python

'''
This script removes orphans reads from paired reads FastQ files.

Authors: Pinosio, Magris, Zamboni
Contact: pinosio@appliedgenomics.org

13-05-03 (Zamboni): Added compression as option (requested by Scalabrin)
'''

import argparse
import subprocess
import sys


def parseFastq(file_handler):
    read_text = ''
    read_id = ''
    try:
        read_text += file_handler.readline().strip()
        if read_text:
            read_id = read_text.split(' ')[0][1:]
            read_text += '\n' + file_handler.readline()
            read_text += file_handler.readline()
            read_text += file_handler.readline()
    except:
        pass
    return {'id': read_id, 'text': read_text}


def create_pool(file_name, pool, pool_size, **kwargs):
    while len(pool) < pool_size:
        r = parseFastq(file_name)
        if not r['id']:
            break
        pool.append(r)


def remove_orphans(input1, input2, output, pool_size, **kwargs):
    compress_output = kwargs.get('compress_output', False)
    verbose = kwargs.get('verbose', False)
    file1 = open(input1)
    file2 = open(input2)
    pref1 = str(output) + "_1.fastq"
    pref2 = str(output) + "_2.fastq"
    prefO = str(output) + "_orphans.fastq"
    output1 = open(pref1, 'w')
    output2 = open(pref2, 'w')
    outputO = open(prefO, 'w')
    pool1 = []
    pool2 = []
    create_pool(file1, pool1, pool_size)
    create_pool(file2, pool2, pool_size)
    read_counter = 0

    if verbose:
        sys.stderr.write('Pools created, start analysis\n')

    p1 = 0
    while pool1 and pool2:
        # compare the first read of pool1 with all reads included in pool2
        try:
            r1 = pool1[p1]
            read_counter += 1
            if read_counter % 1000000 == 0 and verbose:
                sys.stderr.write('Parsed %s reads\r' % read_counter)
        except IndexError:
            # length of pool1 is smaller than pool_size
            break
        r2 = None
        p2 = 0
        if len(pool2) < pool_size:
            create_pool(file2, pool2, pool_size)

        while p2 < pool_size and not r2:
            try:
                r2_temp = pool2[p2]
            except IndexError:
                # rows in pool1 are less than rows in pool2, and
                # the second file is ended
                break
            if r1['id'] == r2_temp['id']:
                r2 = r2_temp
            else:
                p2 += 1

        if r2:
            output1.write(r1['text'])
            output2.write(r2['text'])
        if not r2:
            outputO.write(r1['text'])
        p1 += 1

        if p1 == pool_size:
            # when pool_size is reached a new pool1 is created
            pool1 = []
        if r2:
            # The loop write to orphan.fastq & removes R2 from pool2 and
            # all the reads preceding a match
            while p2 >= 0:
                if p2 != 0:
                    # If R2 is not found in pool1, write R2 to orphan.fastq
                    outputO.write(pool2[0]['text'])
                # The R2 written in orphan.fastq is deleted
                del pool2[0]
                p2 -= 1

        if pool1 == []:
            # When pool1 is empty, a new pool1 is created
            p1 = 0
            create_pool(file1, pool1, pool_size)

        if not pool2:
            # If pool2 is empty, a new pool2 is created
            create_pool(file2, pool2, pool_size)

    for x in pool2:
        outputO.write(x['text'])

    if verbose:
        sys.stderr.write('\n')

    output1.close()
    output2.close()
    outputO.close()
    file1.close()
    file2.close()

    '''
    Output Compression
    '''
    if compress_output:
        try:
            subprocess.check_call(['gzip', str(pref1), str(pref2), str(prefO)])
        except subprocess.CalledProcessError:
            sys.stderr.write("Error during gizp command\n")

if __name__ == "__main__":
    parser = argparse.ArgumentParser(
            description='Remove orphans reads from fastq files')
    parser.add_argument("-v", "--verbose",
                        action="store_true",
                        dest="verbose",
                        default=False,
                        required=False,
                        help="Verbose actions")
    parser.add_argument("-1", "--read1",
                        dest="input1",
                        required=True,
                        help="R1 fastq file")
    parser.add_argument("-2", "--read2",
                        dest="input2",
                        required=True,
                        help="R2 fastq file")
    parser.add_argument("-o", "--output-prefix",
                        dest="prefix",
                        required=True,
                        help="prefix for the output file")
    parser.add_argument("-p", "--pool-size",
                        dest="pool_size",
                        type=int,
                        default=1000,
                        required=False,
                        help="pool size for read comparison (default: 1000)")
    parser.add_argument("-c", "--compress-output",
                        dest="compress_output",
                        action="store_true",
                        default=False,
                        help="Output compression with gzip")

    args = parser.parse_args()

    remove_orphans(args.input1, args.input2, args.prefix, args.pool_size,
                   compress_output=args.compress_output,
                   verbose=args.verbose)
