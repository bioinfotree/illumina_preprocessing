#!/usr/bin/perl
use strict; 
use warnings;
use Data::Dumper;
use Getopt::Std;
use Pod::Usage;
use vars qw($opt_1 $opt_2 $opt_w $opt_o $opt_S $opt_p);
my (%opt);
getopts('1:2:w:o:Sp:'); 
pod2usage(-verbose => 1, -message => "$0: At least two fastq files must be provided \n") if (!$opt_1 || !$opt_2);
my $length = 25;
my $offset = 5;

#getopts('w:o:S:');


$length = $opt_w if $opt_w;
$offset = $opt_o if $opt_o;


if ($opt_1 =~ /\.gz$/) {
    open (F1, "gunzip -c $opt_1 |");
}
else{
    open(F1, $opt_1) or pod2usage("$opt_1: File not found!\n");
}

if ($opt_2 =~ /\.gz$/) {
    open (F2, "gunzip -c $opt_2 |");
}
else{
    open(F2, $opt_2) or pod2usage("$opt_2: File not found!\n");;
}


#my $offset = 0;
#my $length = $ARGV[2];
#my $count = $ARGV[3];

(my $prefix = $opt_1) =~ s/\.gz//;
($prefix = $prefix) =~ s/\.(fq|fastq)//;
($prefix = $prefix) =~ s/_?R[1-3]_?.*//;
($prefix = $prefix) =~ s/_?L[0-9]{3}_?.*//;
print "Using prefix ".$prefix."\n";

$prefix = $opt_p if $opt_p;

open(UNIQUE, ">$prefix\-non_redundant_$length.txt") or die;
open(MULTI,  ">$prefix\-multi_$length.txt" ) or die;

print "Using substrings of ${length}bp\n";
print "Using offset at ${offset}th base\n";

my %d;
my %multi;
my $counter_a;
my $counter_u;
print "Reading fastq files...\n";
while (<F1>) {
    $counter_a++;
	my $h1 = $_;
	my $h2 = <F2>;
	my $s1 = <F1>;
	my $s2 = <F2>;
	my $c1 = <F1>;
	my $c2 = <F2>;
	my $q1 = <F1>;
	my $q2 = <F2>;

	last if not defined $h1;

	my $v1 = substr($s1, $offset, $length);
	my $v2 = substr($s2, $offset, $length);
	
    if (not exists $d{$v1.$v2}) {
        $counter_u++;
        $d{$v1.$v2} = $h1; # here collect names of reads with sequence signature encountered for the first time
    } 
	
    else {
        $multi{$v1.$v2}++
    }		

	
}
close (F1);
close (F2);

my %h;

print "$counter_a pairs processed\n";
print "$counter_u pairs are representative of unique reads\n";
my $ratio = $counter_u/$counter_a*100;
printf "Uniqueness ratio is: %.1f%%\n", $ratio;

foreach my $seq (keys %d) {
	if (exists $multi{$seq}) {
            print MULTI $multi{$seq} +1, "\t", $d{$seq};
            $h{$multi{$seq}}++;
            print UNIQUE $d{$seq};
        } 
        else {
            print UNIQUE $d{$seq};
        }
}


print "Writing duplicates histogram...\n";

open (HIST, ">$prefix\-histogram_$length.txt");
foreach my $val (sort {$b <=> $a} keys %h) {
	print HIST $val+1, "\t", $h{$val}, "\n";
}
%h=();
close (UNIQUE);
close (MULTI);
close (HIST);


map { $_ =~ s/\s.*//} values %d;

#print Dumper \%d;

my %lookup = reverse %d;
%d=();


if ($opt_S) {
    print "Extracting reads....\n";
    if ($opt_1 =~ /\.gz$/) {
        open (F1, "gunzip -c $opt_1 |");
    }
    else{
        open(F1, $opt_1);
    }

    if ($opt_2 =~ /\.gz$/) {
        open (F2, "gunzip -c $opt_2 |");
    }
    else{
        open(F2, $opt_2);
    }
    
    open (OUT1, ">${prefix}_R1_uniq${length}.fastq");
    open (OUT2, ">${prefix}_R2_uniq${length}.fastq");
    
    my $key;
    while (<F1>) {

        my $h1 = $_;
        my $h2 = <F2>;
        my $s1 = <F1>;
        my $s2 = <F2>;
        my $c1 = <F1>;
        my $c2 = <F2>;
        my $q1 = <F1>;
        my $q2 = <F2>;
        
        ($key = $h1) =~ s/\s.*//;
        #print $key."\n";
        if (exists $lookup{$key}) {
            print OUT1 $h1.$s1.$c1.$q1;
            print OUT2 $h2.$s2.$c2.$q2;
        }
    }
}
__END__


=head1 NAME

Script for calculating duplicate reads on the basis of identical bases of read 1 and 2 of PEs

=head1 SYNOPSIS

Usage: filter_duplicates.pl -1 <read1> -2 <read2> [-w (int)] [-S]

  -1          Read 1 fastq file (gzip files readable too)
  -2          Read 2 fastq file (gzip files readable too)
  -w [int]    Word size used for each read [default: 25bp]
  -o [int]    Offset of strings  [default: 5bp]
  -S          Perform extraction of uniquely representative reads
  -p          Prefix of out output files (default: guessing)


