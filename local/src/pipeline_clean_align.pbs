#!/bin/bash
#PBS -N pipeline_test
#PBS -r n
#PBS -l walltime=48:00:00
#PBS -l nodes=1:ppn=4,vmem=15500mb
#PBS -m ae
#PBS -M mvidotto@appliedgenomics.org


module load sw/bio/tom/trunk
module load lang/python/2.7.3
module load lang/perl/5.10.1
module load sw/aligners/erne/1.2
module load sw/aligners/bwa/0.6.2
module load sw/bio/samtools/0.1.18
module load sw/bio/ucsc-utils/default
module load sw/bio/iga_tools/default


cd $PBS_O_WORKDIR

query1=/projects/novabreed/share/mvidotto/bioinfotree/prj/illumina_preprocessing/dataset/simtest/phase_1/reads_1.fq.gz
query2=/projects/novabreed/share/mvidotto/bioinfotree/prj/illumina_preprocessing/dataset/simtest/phase_1/reads_2.fq.gz
contaminant=/projects/novabreed/share/mvidotto/bioinfotree/prj/illumina_preprocessing/dataset/test/phase_1/contaminats.eht
genome_size=486198630
reference=/genomes/vitis_vinifera/assembly/reference/12xCHR/vitis_vinifera_12xCHR.fasta
adapter1=CTGTCTCTTATACACATCTCCGAGCCCACGAGACCGTACTAGATCTCGTATGCCGTCTTCTGCTTG
adapter2=CTGTCTCTTATACACATCTGACGCTGCCGACGATATGCAGTGTGTAGATCTCGGTGGTCGCCGTATCATT

#erne-filter (trimming)
zcat $query1 | perl /home/scalabrin/script/sequence/fastq_coverage.pl --fastq - --genome_size $genome_size --duplicate 1
erne-filter --force-standard --query1 $query1 --query2 $query2 --output-prefix ${PBS_JOBNAME}_trimmed --threads 4 --min-size 50
cat ${PBS_JOBNAME}_trimmed_1.fastq ${PBS_JOBNAME}_trimmed_2.fastq | perl /home/scalabrin/script/sequence/fastq_coverage.pl --fastq - --genome_size $genome_size


# cutadapt
cutadapt -b $adapter1 -O 10 -n 2 -m 50 ${PBS_JOBNAME}_trimmed_1.fastq >${PBS_JOBNAME}_cutadapt_1.fastq 2>${PBS_JOBNAME}_cutadapt_1.stats
cutadapt -b $adapter2 -O 10 -n 2 -m 50 ${PBS_JOBNAME}_trimmed_2.fastq >${PBS_JOBNAME}_cutadapt_2.fastq 2>${PBS_JOBNAME}_cutadapt_2.stats
cutadapt -b $adapter1 -b $adapter2 -O 10 -n 2 -m 50 ${PBS_JOBNAME}_trimmed_unpaired.fastq >${PBS_JOBNAME}_cutadapt_unpaired.fastq 2>${PBS_JOBNAME}_cutadapt_unpaired.stats
rm ${PBS_JOBNAME}_trimmed_1.fastq ${PBS_JOBNAME}_trimmed_2.fastq ${PBS_JOBNAME}_trimmed_unpaired.fastq


# remove orphans
remove_orphans.py -v -1 ${PBS_JOBNAME}_cutadapt_1.fastq -2 ${PBS_JOBNAME}_cutadapt_2.fastq -o ${PBS_JOBNAME}_cutadapt_paired
rm ${PBS_JOBNAME}_cutadapt_1.fastq ${PBS_JOBNAME}_cutadapt_2.fastq
cat ${PBS_JOBNAME}_cutadapt_paired_1.fastq ${PBS_JOBNAME}_cutadapt_paired_2.fastq | perl /home/scalabrin/script/sequence/fastq_coverage.pl --fastq - --genome_size $genome_size


#erne-filter (filtering)
erne-filter --force-standard --query1 ${PBS_JOBNAME}_cutadapt_paired_1.fastq --query2 ${PBS_JOBNAME}_cutadapt_paired_2.fastq --output-prefix ${PBS_JOBNAME}_filtered --contamination-reference $contaminant --threads 4 --min-size 50
rm ${PBS_JOBNAME}_cutadapt_paired_1.fastq ${PBS_JOBNAME}_cutadapt_paired_2.fastq
cat ${PBS_JOBNAME}_filtered_1.fastq ${PBS_JOBNAME}_filtered_2.fastq | perl /home/scalabrin/script/sequence/fastq_coverage.pl --fastq - --genome_size $genome_size
mv ${PBS_JOBNAME}_filtered_1.fastq ${PBS_JOBNAME}_1.fastq #<REMAIN
mv ${PBS_JOBNAME}_filtered_2.fastq ${PBS_JOBNAME}_2.fastq #<REMAIN


# merge unpaired/orphans
erne-filter --force-standard --query1 ${PBS_JOBNAME}_cutadapt_unpaired.fastq --output-prefix ${PBS_JOBNAME}_cutadapt_filtered --contamination-reference $contaminant --threads 4 --min-size 50
rm ${PBS_JOBNAME}_cutadapt_unpaired.fastq
erne-filter --force-standard --query1 ${PBS_JOBNAME}_cutadapt_paired_orphans.fastq --output-prefix ${PBS_JOBNAME}_orphans_filtered --contamination-reference $contaminant --threads 4 --min-size 50
rm ${PBS_JOBNAME}_cutadapt_paired_orphans.fastq
cat ${PBS_JOBNAME}_cutadapt_filtered_1.fastq ${PBS_JOBNAME}_orphans_filtered_1.fastq ${PBS_JOBNAME}_filtered_unpaired.fastq >${PBS_JOBNAME}_unpaired.fastq #<REMAIN
rm ${PBS_JOBNAME}_cutadapt_filtered_1.fastq ${PBS_JOBNAME}_orphans_filtered_1.fastq ${PBS_JOBNAME}_filtered_unpaired.fastq
perl /home/scalabrin/script/sequence/fastq_coverage.pl --fastq ${PBS_JOBNAME}_unpaired.fastq --genome_size $genome_size


## bwa alignment
#bwa aln -t 4 $reference ${PBS_JOBNAME}_1.fastq > ${PBS_JOBNAME}_1.sai
#bwa aln -t 4 $reference ${PBS_JOBNAME}_2.fastq > ${PBS_JOBNAME}_2.sai
#bwa aln -t 4 $reference ${PBS_JOBNAME}_unpaired.fastq > ${PBS_JOBNAME}_unpaired.sai


## sampe
#bwa sampe $reference ${PBS_JOBNAME}_1.sai ${PBS_JOBNAME}_2.sai ${PBS_JOBNAME}_1.fastq ${PBS_JOBNAME}_2.fastq > ${PBS_JOBNAME}.sam
#rm ${PBS_JOBNAME}_1.sai ${PBS_JOBNAME}_2.sai


## samse
#bwa samse $reference ${PBS_JOBNAME}_unpaired.sai ${PBS_JOBNAME}_unpaired.fastq > ${PBS_JOBNAME}_unpaired.sam
#rm ${PBS_JOBNAME}_unpaired.sai
#gzip ${PBS_JOBNAME}_1.fastq&
#gzip ${PBS_JOBNAME}_2.fastq&
#gzip ${PBS_JOBNAME}_unpaired.fastq&
#samtools view -S ${PBS_JOBNAME}_unpaired.sam >> ${PBS_JOBNAME}.sam
#rm ${PBS_JOBNAME}_unpaired.sam

## uniq
#samtools view -S -H ${PBS_JOBNAME}.sam > ${PBS_JOBNAME}_uniq.sam
#samtools view -S -h ${PBS_JOBNAME}.sam | grep "XT:A:U" >> ${PBS_JOBNAME}_uniq.sam


## sam to bam conversion
##samtools view -bS ${PBS_JOBNAME}_unpaired.sam > ${PBS_JOBNAME}_unpaired.bam
##rm ${PBS_JOBNAME}_unpaired.sam
#samtools view -bS ${PBS_JOBNAME}.sam > ${PBS_JOBNAME}.bam
#rm ${PBS_JOBNAME}.sam
#samtools view -bS ${PBS_JOBNAME}_uniq.sam > ${PBS_JOBNAME}_uniq.bam
#rm ${PBS_JOBNAME}_uniq.sam


## sort bam
#samtools sort ${PBS_JOBNAME}.bam ${PBS_JOBNAME}_sort
#mv ${PBS_JOBNAME}_sort.bam ${PBS_JOBNAME}.bam
#samtools sort ${PBS_JOBNAME}_uniq.bam ${PBS_JOBNAME}_uniq_sort
#mv ${PBS_JOBNAME}_uniq_sort.bam ${PBS_JOBNAME}_uniq.bam


## remove duplicates
#samtools rmdup -S ${PBS_JOBNAME}.bam ${PBS_JOBNAME}_nodup.bam
#mv ${PBS_JOBNAME}_nodup.bam ${PBS_JOBNAME}.bam
#samtools rmdup -S ${PBS_JOBNAME}_uniq.bam ${PBS_JOBNAME}_uniq_nodup.bam
#mv ${PBS_JOBNAME}_uniq_nodup.bam ${PBS_JOBNAME}_uniq.bam


## uniquely paired
#samtools view -H ${PBS_JOBNAME}_uniq.bam > ${PBS_JOBNAME}_uniq_paired.sam
#samtools view -h ${PBS_JOBNAME}_uniq.bam | awk '{ if ( $2 == "99" || $2 == "147" || $2 == "83" || $2 == "163" ) {print $0} }' >> ${PBS_JOBNAME}_uniq_paired.sam
#samtools view -bS ${PBS_JOBNAME}_uniq_paired.sam > ${PBS_JOBNAME}_uniq_paired.bam
#rm ${PBS_JOBNAME}_uniq_paired.sam


## compute coverage
#compute_profile --input-file ${PBS_JOBNAME}.bam --sam-format --fasta $reference --output-file ${PBS_JOBNAME}.wig
#compute_profile --input-file ${PBS_JOBNAME}_uniq.bam --sam-format --fasta $reference --output-file ${PBS_JOBNAME}_uniq.wig
#compute_profile --input-file ${PBS_JOBNAME}_uniq_paired.bam --sam-format --fasta $reference --output-file ${PBS_JOBNAME}_uniq_paired.wig
#perl /home/scalabrin/script/illumina/coverage_WIG_sum.pl --WIG ${PBS_JOBNAME}.wig
#perl /home/scalabrin/script/illumina/coverage_WIG_sum.pl --WIG ${PBS_JOBNAME}_uniq.wig
#perl /home/scalabrin/script/illumina/coverage_WIG_sum.pl --WIG ${PBS_JOBNAME}_uniq_paired.wig
