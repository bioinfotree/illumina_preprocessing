#!/bin/bash 

#Perform alignments and all the tasks that Sara does...

#Takes five parameters, see example below


#inerne1=/projects/novabreed/lanes/sequences/dna/zea_mays/H99/exp_184_mais-h99_ACAGTG_L003_R1_001.fastq.gz
#inerne2=/projects/novabreed/lanes/sequences/dna/zea_mays/H99/exp_184_mais-h99_ACAGTG_L003_R2_001.fastq.gz
#maskfile=/projects/novabreed/share/marroni/tmp_chloro/chloroplast_v2.eht

#fastafile=/genomes/zea_mays/assembly/reference/v2/zea_mays_v2.fasta
#infile1=/projects/novabreed/lanes/sequences/dna/zea_mays/H99/FC0184/mais_h99_cutadapt_1.fastq
#infile2=/projects/novabreed/lanes/sequences/dna/zea_mays/H99/FC0184/mais_h99_cutadapt_2.fastq
#outdir=/projects/novabreed/lanes/alignments/BWA/dna/zea_mays/H99/


#Test alignment of run 214 performed in January 2013 with Mirko and Gabriele

#maskfile=/projects/novabreed/share/marroni/tmp_chloro/chloroplast_v2.eht
#fastafile=/genomes/zea_mays/assembly/reference/v2/zea_mays_v2.fasta
#outdir=/projects/novabreed/lanes/alignments/BWA/dna/zea_mays/H99/



#Show usage subroutine
show_usage() {
echo "Usage: ${0##/*} ref_file read1 read2 contaminants output_dir"
echo
echo "ref_file: reference file in fasta format"
echo "read1: name of fastq file containing first reads of the pairs (es: s_1_1_sequence.txt)"
echo "read2: name of fastq file containing second reads of the pairs (es: s_1_2_sequence.txt)"
echo "contaminants: chloroplast and/or mitochondria sequences (indexed for erne)"
echo "output_dir: directory for alignment files "
echo
exit
}

# Minimum number of arguments needed by this program
MINARGS=5

# get the number of command-line arguments given
ARGC=$#

# check to make sure enough arguments were given or exit
if [[ $ARGC -lt $MINARGS ]] ; then
 echo
 show_usage
fi


fastafile=$1
inerne1=$2
inerne2=$3
maskfile=$4
outdir=$5


## Pay attention to the file-name: if it is a SRA file should have only _1, 
## when it is an illumina output file it ends with R1_001 [choose one of the options available] 
outerne=${inerne1/_1/}
#outerne=${inerne1/_R1/}
outpref=${outerne/.fastq.*/}
seqdir=`dirname $outpref`
tlane=`basename $outerne` 
LANE=${tlane/.fastq.*/}
var=`basename $seqdir`
variety=$var"_"$LANE

trimdir=${seqdir}/trimmed
mkdir -p $trimdir


nome_file=`basename $inerne1` \
nomefile=$nome_file \
nlines=$(bzcat $inerne1 | wc -l) \
echo -e $nomefile "\t" $nlines >> /projects/novabreed/share/gmagris/allineamenti_mais/$variety.txt

nome_file=`basename $inerne2` \
nomefile=$nome_file \
nlines=$(bzcat $inerne2 | wc -l) \
echo -e $nomefile "\t" $nlines >> /projects/novabreed/share/gmagris/allineamenti_mais/$variety.txt


#Use erne to remove chloroplast
erne-filter --query1 $inerne1 --query2 $inerne2 --output-prefix ${outpref}_trimmed --contamination-reference $maskfile --threads 8 --min-size 50

nome_file=${LANE}_trimmed_1.fastq \
nomefile="#"$nome_file \
nlines=$(cat ${outpref}_trimmed_1 | wc -l) \
echo -e $nomefile "\t" $nlines >> /projects/novabreed/share/gmagris/allineamenti_mais/$variety.txt

nome_file=${LANE}_trimmed_2.fastq \
nomefile="#"$nome_file \
nlines=$(cat ${outpref}_trimmed_2.fastq | wc -l) \
echo -e $nomefile "\t" $nlines >> /projects/novabreed/share/gmagris/allineamenti_mais/$variety.txt

nome_file=${LANE}_trimmed_unpaired.fastq \
nomefile=$nome_file \
nlines=$(cat ${outpref}_trimmed_unpaired.fastq | wc -l) \
echo -e $nomefile "\t" $nlines >> /projects/novabreed/share/gmagris/allineamenti_mais/$variety.txt


cutadapt -b AGATCGGAAGAGCACACGTCTGAACTCCAGTCACTTCGCTATCTCGTATGCCGTCTTCTGCTTG -O 10 -n 2 -m 50 ${outpref}_trimmed_1.fastq > ${trimdir}/${LANE}_cutadapt_1.fastq 2>  ${trimdir}/${LANE}_cutadapt_1.stats
cutadapt -b AAGATCGGAAGAGCGTCGTGTAGGGAAAGAGTGTAGATCTCGGTGGTCGCCGTATCATT -O 10 -n 2 -m 50 ${outpref}_trimmed_2.fastq > ${trimdir}/${LANE}_cutadapt_2.fastq 2>  ${trimdir}/${LANE}_cutadapt_2.stats

nome_file=${LANE}_cutadapt_1.fastq \
nomefile="#"$nome_file \
nlines=$(cat ${trimdir}/${LANE}_cutadapt_1.fastq | wc -l) \
echo -e $nomefile "\t" $nlines >> /projects/novabreed/share/gmagris/allineamenti_mais/$variety.txt

nome_file=${LANE}_cutadapt_2.fastq \
nomefile="#"$nome_file \
nlines=$(cat ${trimdir}/${LANE}_cutadapt_2.fastq | wc -l) \
echo -e $nomefile "\t" $nlines >> /projects/novabreed/share/gmagris/allineamenti_mais/$variety.txt



cutadapt -b AGATCGGAAGAGCACACGTCTGAACTCCAGTCACTTCGCTATCTCGTATGCCGTCTTCTGCTTG -O 10 -n 2 -m 50 ${outpref}_trimmed_unpaired.fastq > ${trimdir}/${LANE}_tmp.fastq 2>  ${trimdir}/${LANE}_cutadapt_unpaired_tmp.stats
cutadapt -b AAGATCGGAAGAGCGTCGTGTAGGGAAAGAGTGTAGATCTCGGTGGTCGCCGTATCATT -O 10 -n 2 -m 50 ${trimdir}/${LANE}_tmp.fastq > ${trimdir}/${LANE}_cutadapt_unpaired.fastq 2>  ${trimdir}/${LANE}_cutadapt_unpaired.stats

nome_file=${LANE}_tmp.fastq \
nomefile=$nome_file \
nlines=$(cat ${trimdir}/${LANE}_tmp.fastq | wc -l) \
echo -e $nomefile "\t" $nlines >> /projects/novabreed/share/gmagris/allineamenti_mais/$variety.txt

nome_file=${LANE}_cutadapt_unpaired.fastq \
nomefile=$nome_file \
nlines=$(cat ${trimdir}/${LANE}_cutadapt_unpaired.fastq | wc -l) \
echo -e $nomefile "\t" $nlines >> /projects/novabreed/share/gmagris/allineamenti_mais/$variety.txt


rm ${trimdir}/${LANE}_tmp.fastq

rm ${outpref}_trimmed_1.fastq
rm ${outpref}_trimmed_2.fastq
rm ${outpref}_trimmed_unpaired.fastq

# New remove_orphans in python which create 3 different files in output _1.fastq, _2.fastq and _orphans.fastq
remove_orphans.py -1 ${trimdir}/${LANE}_cutadapt_1.fastq -2 ${trimdir}/${LANE}_cutadapt_2.fastq -o ${trimdir}/${LANE}

nome_file=${LANE}_1.fastq \
nomefile="#"$nome_file \
nlines=$(cat ${trimdir}/${LANE}_1.fastq | wc -l) \
echo -e $nomefile "\t" $nlines >> /projects/novabreed/share/gmagris/allineamenti_mais/$variety.txt

nome_file=${LANE}_2.fastq \
nomefile="#"$nome_file \
nlines=$(cat ${trimdir}/${LANE}_2.fastq | wc -l) \
echo -e $nomefile "\t" $nlines >> /projects/novabreed/share/gmagris/allineamenti_mais/$variety.txt


##verify if the file change!
#mv ${trimdir}/${LANE}_cutadapt_paired_1.fastq ${trimdir}/${LANE}_cutadapt_1.fastq
#mv ${trimdir}/${LANE}_cutadapt_paired_2.fastq ${trimdir}/${LANE}_cutadapt_2.fastq


infile1=${trimdir}/${LANE}_1.fastq
infile2=${trimdir}/${LANE}_2.fastq
infileu=${trimdir}/${LANE}_cutadapt_unpaired.fastq
saifile1=`basename $infile1 .fastq`.sai
saifile2=`basename $infile2 .fastq`.sai
saifileu=`basename $infileu .fastq`.sai
samfile=${saifile1/_1.sai/_paired.sam}
samfileu=${saifileu/.sai/.sam}
bamfile=${saifile1/_1.sai/_paired.bam}
bamfileu=${saifileu/.sai/.bam}
bamall=${bamfileu/_unpaired/_all}
sortfile=${bamfile/.bam/_sort}


cd $outdir

#echo "Begin alignment!"
#echo "Reference =" ${fastafile}
#echo "Input (fastq) file =" ${infile1}
#echo "Output (sai) file =" ${saifile1}

#bwa aln -t 4 ${fastafile} ${infile1} > ${saifile1}
#bwa aln -t 4 ${fastafile} ${infile2} > ${saifile2}
#bwa aln -t 4 $fastafile $infileu > $saifileu
bwa aln -t 8 ${fastafile} ${infile1} > ${saifile1}
bwa aln -t 8 ${fastafile} ${infile2} > ${saifile2}
bwa aln -t 8 $fastafile $infileu > $saifileu

tlane=`basename $inerne1` 
LANE=${tlane/.fastq.gz/}
bwa sampe -r "@RG\tID:$LANE\tPL:illumina\tPU:$LANE\tLB:$LANE\tSM:$LANE" $fastafile $saifile1 $saifile2 $infile1 $infile2 > $samfile
bwa samse -r "@RG\tID:$LANE\tPL:illumina\tPU:$LANE\tLB:$LANE\tSM:$LANE" $fastafile $saifileu $infileu > $samfileu
samtools import $fastafile $samfile $bamfile
samtools import $fastafile $samfileu $bamfileu
samtools merge $bamall $bamfile $bamfileu

#PL:illumina	PU:HP301_GGATGT_R1_001	LB:HP301_GGATGT_R1_001
##modifie Header of bam file (in order to prevent problems with breakdancer) adding more information such as PL, PU and LB

# Alignement will be sorted on the base of the coordinates (sort degli allineamenti in base alle coordinate)
samtools sort $bamall $sortfile

#Remove (PCR) duplicates
nodupfile=${sortfile/_paired_sort/_nodup}
duptxt=${nodupfile}.txt
samtools rmdup ${sortfile}.bam ${nodupfile}.bam 2>$duptxt
mv ${nodupfile}.bam ${sortfile}.bam

#Creo un file sam in cui ci sono solo le reads allineate in modo unico 
usamfile=${sortfile/_cutadapt_paired_sort/_U}".sam"
ubamfile=${usamfile/_U.sam/_U.bam}
usortfile=${ubamfile/_U.bam/_sort}
statfile="mapping_"${usortfile/_sort/.txt}
insertstatfile=${samfile/.sam/_insertsize.txt}

#Extract insert size info and write to file (we will then extract stats with R)
samtools view ${sortfile}.bam | cut -f9 | sort | uniq -c > $insertstatfile
rm $samfile
#rm $bamfile

#Index sorted alignment for fast random access/create a file .bai
samtools index ${sortfile}.bam

#Creo header
samtools view -H ${sortfile}.bam > $usamfile

samtools view ${sortfile}.bam | awk '{ if ( $13 == "XT:A:U" ) {print $0} }' >> $usamfile

samtools import $fastafile $usamfile $ubamfile
samtools view ${ubamfile} | cut -f2 |sort|uniq -c > $statfile
rm $usamfile
#echo Opening file ${ubamfile}
samtools index ${ubamfile}


# Selection of uniquely aligned in read pairs
SAM_PU=${usamfile/_U.sam/_PU.sam}
BAM_PU=${ubamfile/_U.bam/_PU.bam}
samtools view -H $ubamfile > $SAM_PU
samtools view $ubamfile | awk '{ if ( $2 == "99" || $2 == "147" || $2 == "83" || $2 == "163" ) {print $0} }' >> $SAM_PU
samtools view -bS $SAM_PU > $BAM_PU
rm $SAM_PU


#Use qaTools to estimate insert size
/projects/novabreed/share/gmagris/software/qaTools/computeInsertSizeHistogram -s $ubamfile ${ubamfile/.bam/_insert.txt}
/projects/novabreed/share/gmagris/software/qaTools/computeInsertSizeHistogram -s $BAM_PU ${BAM_PU/.bam/_insert.txt}

/projects/novabreed/share/gmagris/software/qaTools/qaCompute $ubamfile ${ubamfile/.bam/_coverage.txt}
/projects/novabreed/share/gmagris/software/qaTools/qaCompute $BAM_PU ${BAM_PU/.bam/_coverage.txt}



#for OLDBAMFILE in ${ubamfile} $BAM_PUù
#for OLDBAMFILE in $bamall 
#for OLDBAMFILE in exp_214_mais-h99_TTCGCT_L006_001_cutadapt_nodup.bam

for OLDBAMFILE in $bamall ${sortfile}.bam ${ubamfile} $BAM_PU
do
mydir=$(pwd)
BAMFILE=${mydir}/${OLDBAMFILE}
/projects/novabreed/share/gmagris/allineamenti_mais/coverage.sh ${fastafile} ${BAMFILE}
COVFILE=${BAMFILE/alignments/coverage}
WIGFILE=${COVFILE/.bam/.wig.gz}
#if [$BAMFILE -ne ${ubamfile}]
#then 
#fi
#rm $WIGFILE
done

#${BAMFILE}=${sortfile}.bam
#/projects/novabreed/share/gmagris/allineamenti_mais/coverage.sh ${fastafile} ${BAMFILE}
#COVFILE=${BAMFILE/alignments/coverage}
#WIGFILE=${COVFILE/.bam/.wig}
#rm $WIGFILE

#/projects/novabreed/share/gmagris/allineamenti_mais/coverage.sh ${fastafile} ${ubamfile}
#/projects/novabreed/share/gmagris/allineamenti_mais/coverage.sh ${fastafile} $BAM_PU

